package com.pearbit.nivel3Capacitacion.main;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;


import com.pearbit.nivel3Capacitacion.controller.UserController;
import com.pearbit.nivel3Capacitacion.model.User;

public class Launch {
	
	public static void main(String[] args) throws SQLException {
		UserController obj= new UserController();
		List<User> list=obj.FindAll();
		
//		List <User> users=Arrays.asList(
//				new User(2,"Francisco","Pass",1,null), 
//				new User(3,"Alberto","Pass",0,null),
//				new User(4,"Salinas","Pass",0,null) 
//				);
//		for (User user:users) {
//			obj.insert(user);
//		}
				
		for(User user: list) {
			System.out.println(user.toString());
		}
		
	}
}
