package com.pearbit.nivel3Capacitacion.controller;

import java.util.List;

import com.pearbit.nivel3Capacitacion.dao.sql.UserDAO;
import com.pearbit.nivel3Capacitacion.model.User;

public class UserController {
private UserDAO userDao = new UserDAO();

public List<User> FindAll() {
	try {
	return userDao.generalRequest();
	}catch (Exception e) {
		return null;
	}
}

public String delete(int user) {
	try {
	return userDao.delete(user);
	} catch (Exception e) {
		return null;
	}
}

public String insert(User u) {
	try {
		return userDao.insert(u);
	} catch (Exception e) {
		return null;
	}
}
}
