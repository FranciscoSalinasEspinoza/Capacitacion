package com.pearbit.nivel5.control;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.pearbit.nivel5.modelo.ModeloPersona;
import com.perabit.nivel5.conexion.SingleDBConnection;

public class ControlPersona {
ModeloPersona modelo;
static SingleDBConnection con;

public ControlPersona(ModeloPersona modelo) {
	this.modelo=modelo;
}
public String InsertarPersona() {
	String instruccion="Insert into persona values(null,"
			+"'"+modelo.getNombre()
			+"','"+modelo.getApellido1()
			+"','"+modelo.getApellido2()
			+"',"+modelo.getEdad()
			+",'"+modelo.getNacionalidad()
			+"','"+modelo.getCorreo()
			+"')";
	 try {
         con = SingleDBConnection.getInstance("nivel5", "root", "pearbit");
         if (con.open()) {
             con.updateData(instruccion);
             
         } else {
             return "Error: Not connected to DB.";
         }
     } catch (ClassNotFoundException ex) {
         return "Error: Driver not loaded.";
     } catch (SQLException ex) {
         return "Error: " + ex.getMessage();
     }
	 return("Correctamente insertado");
}
public static String getTable(String orden) {
    ResultSet data;
    String htmlTable;
    String sql = "select*from persona order by(" +orden
                 +")";
    try {
        con = SingleDBConnection.getInstance("nivel5", "root", "pearbit");
        if (con.open()) {
            data = con.readData(sql);
            htmlTable = printTable(data);
        } else {
            return "ERROR:  Not connected to DB.";
        }
    } catch (ClassNotFoundException ex) {
        return "Error: Driver not loaded.";
    } catch (SQLException ex) {
        return "ERROR: " + ex.getMessage();
    }
    return htmlTable;
}
    


private static String printTable(ResultSet data) 
        throws SQLException {
    String table = "<thead>"
	+"<tr>"
	+"<th>Nombre</th>"
	+"<th>Primer Apellido</th>"
	+"<th>Segundo apellido</th>"
	+"<th>Edad</th>"
	+"<th>Nacionalidad</th>"
	+"<th>Correo</th>"
	+"</tr>"
    +"</thead>"
    +"<tbody>";
    if(!data.first()){
        return "Aún no se ha registrado ninguna persona";
       }
    data.beforeFirst();
    while(data.next()){
        table += "<tr>";
        table += "<td>" + data.getString(2) + "</td> "
              + "<td>" + data.getString(3) + "</td>"
              + "<td>" +data.getString(4)+"</td>"
        	  + "<td>" +data.getString(5)+"</td>"
        	  + "<td>" +data.getString(6)+"</td>"
        	  + "<td>" +data.getString(7)+"</td>";
        table+="</tr>";
    	
        
    }
    
    table+="</tbody>";
    
    return table;
}
}
