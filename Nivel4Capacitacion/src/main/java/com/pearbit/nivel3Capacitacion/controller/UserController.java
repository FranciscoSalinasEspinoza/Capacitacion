package com.pearbit.nivel3Capacitacion.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JOptionPane;

import com.pearbit.nivel3Capacitacion.View.Login;
import com.pearbit.nivel3Capacitacion.dao.sql.UserDAO;
import com.pearbit.nivel3Capacitacion.model.User;

public class UserController implements ActionListener{
private UserDAO userDao = new UserDAO();
private Login viewLogin;
private User user;

public UserController(Login viewLogin) {
	super();
	this.viewLogin=viewLogin;
	viewLogin.onClickbtnVerify(this);
	
	
}



public List<User> FindAll() {
	try {
	return userDao.generalRequest();
	}catch (Exception e) {
		return null;
	}
}



public String delete(int user) {
	try {
	return userDao.delete(user);
	} catch (Exception e) {
		return null;
	}
}

public String insert(User u) {
	try {
		return userDao.insert(u);
	} catch (Exception e) {
		return null;
	}
}



@Override
public void actionPerformed(ActionEvent e) {
	if(e.getSource()==viewLogin.getBtnVerify()) {
		verifyUser();
	}
	
}

private void verifyUser() {
	String userName= viewLogin.getJtfUser().getText();
	String pass=viewLogin.getJtfPass().getText();
	try {
		user=userDao.individualRequest(userName, pass);
		if(user!=null) {
			fillComboRole(user);
		}else {
			JOptionPane.showMessageDialog(null, "Data incorrect, please check the information given", 
					"Error", JOptionPane.ERROR_MESSAGE);
		}
	} catch (Exception e) {
		// TODO: handle exception
	}
	
}

private void fillComboRole(User user) {
	viewLogin.getComboBox().removeAllItems();
	viewLogin.getComboBox().addItem("Available Roles");
	for(int i=0;i<user.getRole().size();i++) {
		viewLogin.getComboBox().addItem(user.getRole().get(i).getName());
	}
}


}
