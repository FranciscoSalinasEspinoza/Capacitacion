package com.pearbit.nivel3Capacitacion.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

@Entity
@Table (name="users")
public class User implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column (name="user_id")
	@GeneratedValue
	private int id;
	
	@Column (name="username")
	private String name;
	
	@Column (name="password")
	private String password;
	
	@Column (name="enable_status")
	private int enable;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL}) //relacion de muchos a muchos 	
	@JoinTable(name="user_role",  //nombre de la tabla relacional
				joinColumns={@JoinColumn(name="user_id")}, //tabla por donde comienza relación 
				inverseJoinColumns={@JoinColumn(name="role_id")}) //otra entidad de relación 
	private List<Role> role;

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(int id, String name, String password, int enable, List<Role> role) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.enable = enable;
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getEnable() {
		return enable;
	}

	public void setEnable(int enable) {
		this.enable = enable;
	}

	public List<Role> getRole() {
		return role;
	}

	public void setRole(List<Role> role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", password=" + password + ", enable=" + enable + ", role=" + role
				+ "]";
	}
	
	
}
