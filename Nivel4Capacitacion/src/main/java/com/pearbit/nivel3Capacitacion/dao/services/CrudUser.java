package com.pearbit.nivel3Capacitacion.dao.services;

import java.sql.SQLException;
import java.util.List;

public interface CrudUser <T>{
	
	public String insert(T t) throws SQLException;
	
	public String edit(T t) throws SQLException;
	
	public String delete(int t)throws SQLException;
	
	public List<T> generalRequest() throws SQLException;
	
	public T  individualRequest (String userName, String pass)throws SQLException;
	
}
