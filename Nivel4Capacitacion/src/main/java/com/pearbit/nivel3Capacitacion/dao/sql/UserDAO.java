package com.pearbit.nivel3Capacitacion.dao.sql;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.pearbit.nivel3Capacitacion.dao.services.CrudUser;
import com.pearbit.nivel3Capacitacion.model.User;
import com.pearbit.nivel3Capacitacion.utils.HibernateUtils;

public class UserDAO implements CrudUser<User>{
	SessionFactory sessionFactory= HibernateUtils.getSessionFactory();
	
	@Override
	public String insert(User t) throws SQLException {
		Session session=sessionFactory.openSession();
		try {
			session.beginTransaction();
			session.save(t);
			session.getTransaction().commit();
		
			return "Se insertó correctamente";
			
			
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			throw new SQLException("Existe un problema con la base de datos \n" + "Error al insertar.");
		}finally {
			session.close();
		}
		
		
	}
	
	@Override
	public String edit(User t) throws SQLException {
		Session session=sessionFactory.openSession();
		try {
			session.beginTransaction();
			User r= session.get(User.class, t.getName());
			r=t;
			session.merge(r);//This will save the instruction in a temporal memory space
			session.getTransaction().commit();//This is the order that will make the changes to our table
			return "Éxito al actualizar";//this will return a message if the action is correctly applied
			
		} catch (Exception e) {
			session.getTransaction().rollback();//If there's a problem, this line will destroy every changes
			//that could be applied
			e.printStackTrace();
			throw new SQLException("Error al editar "+e.getMessage());
		} finally {
			session.close();//closes the session for avoid conflicts 
		}
		
	}

	@Override
	public List<User> generalRequest() throws SQLException {
		Session session=sessionFactory.openSession();//Opens the session for begin to work
		try {
		session.beginTransaction();
		Query<User> query=session.createQuery("from User");//This HQL makes reference to a SELECT * FROM users,
		//but this is for the mapped object in the hibernate.cnf.xml
		List<User> users=query.list();//Obtains the list
		session.getTransaction().commit();
		return users;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new SQLException("Error al consultar: " + e.getMessage());
		} finally {
			session.close();
		}
	}

	@Override
	public User individualRequest(String userName, String pass) throws SQLException {
		Session session=sessionFactory.openSession();
		try {
			session.beginTransaction();
			User user;
			Query query=session.createQuery("from User where username= :username and password= :password");
			query.setString("username", userName);
			query.setString("password", pass);
			user=(User)query.uniqueResult();
			session.getTransaction().commit();
			
			return user;
		} catch (Exception e) {
			e.printStackTrace();
			throw new SQLException("Error al consultar: " + e.getMessage());
		} finally {
			session.close();
		}
	}

	@Override
	public String delete(int t) throws SQLException {
		Session session= sessionFactory.openSession();
		try {
			session.beginTransaction();
			User user= session.get(User.class, t);
			session.delete(user);
			session.getTransaction().commit();
			return "The user was deleted correctly";
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
			throw new SQLException("Error al eliminar: " + e.getMessage());
		} finally {
			session.close();
		}
	}

}
