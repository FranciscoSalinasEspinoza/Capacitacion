package com.pearbit.nivel3Capacitacion.View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField jtfUser;
	private JPasswordField jtfPass;
	private JButton btnVerify;
	private JComboBox comboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 750, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblWelcomoToMy = new JLabel("WELCOME");
		lblWelcomoToMy.setFont(new Font("FreeMono", Font.BOLD, 40));
		lblWelcomoToMy.setBounds(250, 31, 275, 57);
		panel.add(lblWelcomoToMy);
		
		JLabel lblUserName = new JLabel("User Name");
		lblUserName.setBounds(76, 141, 115, 15);
		panel.add(lblUserName);
		
		jtfUser = new JTextField();
		jtfUser.setBounds(350, 137, 224, 40);
		panel.add(jtfUser);
		jtfUser.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(76, 235, 97, 15);
		panel.add(lblPassword);
		
		btnVerify = new JButton("Verify");
		btnVerify.setBounds(76, 294, 117, 25);
		panel.add(btnVerify);
		
		jtfPass = new JPasswordField();
		jtfPass.setBounds(350, 226, 224, 33);
		panel.add(jtfPass);
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Available Roles:"}));
		comboBox.setBounds(350, 294, 224, 40);
		panel.add(comboBox);
	}

	public void onClickbtnVerify(ActionListener l) {
		btnVerify.addActionListener(l);
	}
	
	public JComboBox getComboBox() {
		return comboBox;
	}

	public void setComboBox(JComboBox comboBox) {
		this.comboBox = comboBox;
	}

	public JTextField getJtfUser() {
		return jtfUser;
	}

	public void setJtfUser(JTextField jtfUser) {
		this.jtfUser = jtfUser;
	}

	public JPasswordField getJtfPass() {
		return jtfPass;
	}

	public void setJtfPass(JPasswordField jtfPass) {
		this.jtfPass = jtfPass;
	}

	public JButton getBtnVerify() {
		return btnVerify;
	}

	public void setBtnVerify(JButton btnVerify) {
		this.btnVerify = btnVerify;
	}
	
	
}
