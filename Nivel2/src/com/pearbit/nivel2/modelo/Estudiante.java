package com.pearbit.nivel2.modelo;

import java.util.ArrayList;

public class Estudiante extends Persona{
	String matricula,generacion, horario;
	public Estudiante() {
		super();
	}

	public Estudiante(String matricula, String generacion, String horario) {
		super();
		this.matricula = matricula;
		this.generacion = generacion;
		this.horario = horario;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getGeneracion() {
		return generacion;
	}

	public void setGeneracion(String generacion) {
		this.generacion = generacion;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}
	
}
