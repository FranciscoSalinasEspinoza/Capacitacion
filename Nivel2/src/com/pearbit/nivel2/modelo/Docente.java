package com.pearbit.nivel2.modelo;

public class Docente extends Persona{
	String cedula,especialidad;
	int noCubiculo;
	
	
	public Docente() {
		super();
	}
	public Docente(String cedula, String especialidad, int noCubiculo) {
		super();
		this.cedula = cedula;
		this.especialidad = especialidad;
		this.noCubiculo = noCubiculo;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getEspecialidad() {
		return especialidad;
	}
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	public int getNoCubiculo() {
		return noCubiculo;
	}
	public void setNoCubiculo(int noCubiculo) {
		this.noCubiculo = noCubiculo;
	}
	
}
