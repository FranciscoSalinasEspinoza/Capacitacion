package com.pearbit.nivel2.principal;

import com.pearbit.nivel2.control.Control;
import com.pearbit.nivel2.vista.VistaPersonas;

public class main {

	public static void main(String[] args) {
		VistaPersonas vista= new VistaPersonas();
		Control c= new Control(vista);
		vista.setVisible(true);

	}

}
