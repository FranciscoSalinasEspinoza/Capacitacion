package com.pearbit.nivel2.control;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import com.pearbit.nivel2.modelo.Docente;
import com.pearbit.nivel2.modelo.Estudiante;
import com.pearbit.nivel2.vista.VistaPersonas;
import java.util.regex.*;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
public class Control implements ActionListener, ItemListener {
	
	Estudiante modeloEstudiante;
	Docente modeloDocente;
	VistaPersonas vista;
	ArrayList <String>listaEstudiante =new ArrayList<String>();
	ArrayList <String>listaDocente=new ArrayList<String>();
	private String tipo;

	public Control(VistaPersonas vista) {
		this.vista=vista;
		modeloEstudiante=new Estudiante();
		modeloDocente=new Docente();
		vista.onClickAgregar(this);
		vista.onclickMostrar(this);
		vista.onItemStateChangeCombo(this);
	}
	@Override
	public void itemStateChanged(ItemEvent e) {//Este método ayuda a decidir cuales campos se van a activar 
		//En la vista
		if(e.getSource()==vista.getComboBox()) {
			if(vista.getComboBox().getSelectedItem().toString().equalsIgnoreCase("Docente")) {
				tipo="Docente";
				escoger();
			}else if(vista.getComboBox().getSelectedItem().toString().equalsIgnoreCase("Estudiante")) {
				tipo="Estudiante";
				escoger();
			}else if(vista.getComboBox().getSelectedItem().toString().equalsIgnoreCase("Seleccionar")) {
				tipo="Seleccionar";
				escoger();
			}
		}
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==vista.getJbtAgregar()) {
			if(escoger().equals("Docente")) {
				llenarDatosDocente();
			}else if(escoger().equals("Estudiante")) {
				if(validarMatricula()&&validarHorario()) {
					llenarDatosEstudiante();;
				}else {
					System.out.println("Falso");
				}
				
			}
		}else if(e.getSource()==vista.getJbtMostrarTodos()){
			mostrarDocente();
			mostrarEstudiante();
		}
		
	}
	private void llenarDatosEstudiante() {//Guarda los datos en las variables del modelo estudiante
		modeloEstudiante.setNombre(vista.getJtfNombre().getText());
		modeloEstudiante.setPrimerApellido(vista.getJtfApellido1().getText());
		modeloEstudiante.setSegundoApellido(vista.getJtfApellido2().getText());
		modeloEstudiante.setEdad(Integer.parseInt(vista.getJtfEdad().getText()));
		modeloEstudiante.setMatricula(vista.getJtfMatricula().getText());
		modeloEstudiante.setGeneracion(vista.getJtfGeneracion().getText());
		modeloEstudiante.setHorario(vista.getJtfHorario().getText());
		insertarEstudiante();
	}
	private void insertarEstudiante() {//Inserta los datos guardados en el modelo de estudiante
		String datosEstudiantes=modeloEstudiante.getNombre();
		datosEstudiantes+=" "+modeloEstudiante.getPrimerApellido();
		datosEstudiantes+=" "+modeloEstudiante.getSegundoApellido();
		datosEstudiantes+=" "+modeloEstudiante.getEdad();
		datosEstudiantes+=" "+modeloEstudiante.getMatricula();
		datosEstudiantes+=" "+modeloEstudiante.getGeneracion();
		datosEstudiantes+=" "+modeloEstudiante.getHorario();
		
		listaEstudiante.add(datosEstudiantes);
		JOptionPane.showMessageDialog(null, "Estudiante Agregado", "ok", JOptionPane.INFORMATION_MESSAGE);
		limpiar();
	}

	private void mostrarEstudiante() {//Muestra los datos guardados en la lista de estudiantes

		DefaultListModel modelo= new DefaultListModel();
		for(int i=0;i<listaEstudiante.size();i++) {
			modelo.addElement(listaEstudiante.get(i));
		}

		vista.getJtaEstudiante().setModel(modelo);
	}
	private void llenarDatosDocente() {//Llena las variables del modelo de docente
		modeloDocente.setNombre(vista.getJtfNombre().getText());
		modeloDocente.setPrimerApellido(vista.getJtfApellido1().getText());
		modeloDocente.setSegundoApellido(vista.getJtfApellido2().getText());
		modeloDocente.setEdad(Integer.parseInt(vista.getJtfEdad().getText()));
		modeloDocente.setCedula(vista.getJtfCedula().getText());
		modeloDocente.setEspecialidad(vista.getJtfEspecialidad().getText());
		modeloDocente.setNoCubiculo(Integer.parseInt(vista.getJtfCubiculo().getText()));
		
		insertarDocente();
		
	}
	private void insertarDocente() {//Inserta los datos obtenidos del modelo de la clase docente
		String datosDocente=modeloDocente.getNombre();
		datosDocente+=" "+modeloDocente.getPrimerApellido();
		datosDocente+=" "+modeloDocente.getSegundoApellido();
		datosDocente+=" "+modeloDocente.getEdad();
		datosDocente+=" "+modeloDocente.getCedula();
		datosDocente+=" "+modeloDocente.getEspecialidad();
		datosDocente+=" "+modeloDocente.getNoCubiculo();
		
		listaDocente.add(datosDocente);
		JOptionPane.showMessageDialog(null, "Docente agregaro", "ok", JOptionPane.INFORMATION_MESSAGE);
		limpiar();
	}
	private void  mostrarDocente() {//Muestra a los docentes registrados
		DefaultListModel modelo= new DefaultListModel();
		for(int i=0;i<listaDocente.size();i++) {
			modelo.addElement(listaDocente.get(i));
		}

		vista.getJtaDocente().setModel(modelo);
	}
	private String escoger() {
		if(tipo.equals("Docente")){//Hace visibles los campos de docente y le quita la visibilidad a los campos
			//de  estudiante
			vista.getJtfCubiculo().setVisible(true);
			vista.getJtfCedula().setVisible(true);
			vista.getJtfEspecialidad().setVisible(true);
			vista.getLblCubculo().setVisible(true);
			vista.getLblEspecialidad().setVisible(true);
			vista.getLblCedula().setVisible(true);
			vista.getJbtAgregar().setVisible(true);
			vista.getJtfNombre().setEnabled(true);
			vista.getJtfApellido1().setEnabled(true);
			vista.getJtfApellido2().setEnabled(true);
			vista.getJtfEdad().setEnabled(true);
			
			vista.getJtfGeneracion().setVisible(false);
			vista.getJtfHorario().setVisible(false);
			vista.getJtfMatricula().setVisible(false);
			vista.getLblHorario().setVisible(false);
			vista.getLblGeneracion().setVisible(false);
			vista.getLblMatricula().setVisible(false);
			
		}else if(tipo.equalsIgnoreCase("Estudiante")) {//Hace visibles los campos  de estudiante y le quita 
			//la visibilidad a los campos de docente
			vista.getJtfGeneracion().setVisible(true);
			vista.getJtfHorario().setVisible(true);
			vista.getJtfMatricula().setVisible(true);
			vista.getLblHorario().setVisible(true);
			vista.getLblGeneracion().setVisible(true);
			vista.getLblMatricula().setVisible(true);
			vista.getJbtAgregar().setVisible(true);
			vista.getJtfNombre().setEnabled(true);
			vista.getJtfApellido1().setEnabled(true);
			vista.getJtfApellido2().setEnabled(true);
			vista.getJtfEdad().setEnabled(true);
			
			vista.getJtfCubiculo().setVisible(false);
			vista.getJtfCedula().setVisible(false);
			vista.getJtfEspecialidad().setVisible(false);
			vista.getLblCubculo().setVisible(false);
			vista.getLblEspecialidad().setVisible(false);
			vista.getLblCedula().setVisible(false);
			
		}else if(tipo.equals("Seleccionar")) {
			vista.ocultar();
		}
		return tipo;
	}
	private boolean validarMatricula(){//Usa expresiones regulares para verificar que el 
		//Formato de la matrícula sea el correcto
		String matricula=vista.getJtfMatricula().getText();
		Pattern pat=Pattern.compile("[A-Z a-z]{3}[0-9]{3}[A-Z a-z]{3}");
		Matcher mat=pat.matcher(matricula);
		if(mat.find()) {
		return true;
		}
		else {
			JOptionPane.showMessageDialog(null, "El formato de matrícula no es correcto", 
					"Error", JOptionPane.ERROR_MESSAGE);
			
		}return false;
	}
	private boolean validarHorario() {//Usa expresiones regulares para verificar que el formato del horario
		//Sea correcto
		String horario=vista.getJtfHorario().getText();
		Pattern pat=Pattern.compile("([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])");
		Matcher mat=pat.matcher(horario);
		if(mat.find()) {
			return true;
		}else {
			JOptionPane.showMessageDialog(null, "El formato de Horario no es correcto", 
					"Error", JOptionPane.ERROR_MESSAGE);
		}
		return false;
	}
	private void limpiar() {//Este método limpia los campos de texto de la ventana
		vista.getJtfNombre().setText("");
		vista.getJtfApellido1().setText("");
		vista.getJtfApellido2().setText("");
		vista.getJtfMatricula().setText("");
		vista.getJtfCedula().setText("");
		vista.getJtfCubiculo().setText("");
		vista.getJtfEdad().setText("");
		vista.getJtfEspecialidad().setText("");
		vista.getJtfGeneracion().setText("");
		vista.getJtfHorario().setText("");
		
	}
}
