package com.pearbit.nivel2.vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JList;

public class VistaPersonas extends JFrame {

	private JPanel contentPane;
	private JTextField jtfNombre;
	private JTextField jtfApellido1;
	private JTextField jtfApellido2;
	private JTextField jtfEdad;
	private JTextField jtfMatricula;
	private JTextField jtfGeneracion;
	private JTextField jtfHorario;
	private JTextField jtfCedula;
	private JTextField jtfEspecialidad;
	private JTextField jtfCubiculo;
	private JComboBox comboBox;
	private JLabel lblEstudiantes;
	private JLabel lblDocentes;
	private JLabel lblMatricula;
	private JLabel lblGeneracion;
	private JLabel lblHorario;
	private JButton jbtMostrarTodos;
	private JButton jbtAgregar;
	private JLabel lblCedula;
	private JLabel lblEspecialidad;
	private JLabel lblCubculo;
	private JList jtaEstudiante;
	private JList jtaDocente;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaPersonas frame = new VistaPersonas();
					frame.setVisible(true);
					;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaPersonas() {
		setTitle("Registro de personas");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 0, 650, 750);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(176, 196, 222));
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblTipoDePersona = new JLabel("Tipo de persona");
		lblTipoDePersona.setBounds(332, 44, 115, 15);
		panel.add(lblTipoDePersona);
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Seleccionar", "Estudiante", "Docente"}));
		comboBox.setBounds(482, 39, 126, 24);
		panel.add(comboBox);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(27, 90, 70, 15);
		panel.add(lblNombre);
		
		jtfNombre = new JTextField();
		
		jtfNombre.setBounds(115, 88, 114, 19);
		panel.add(jtfNombre);
		jtfNombre.setColumns(10);
		
		JLabel lblApellidoPaterno = new JLabel("Apellido Paterno");
		lblApellidoPaterno.setBounds(263, 90, 126, 15);
		panel.add(lblApellidoPaterno);
		
		jtfApellido1 = new JTextField();
		jtfApellido1.setBounds(407, 88, 114, 19);
		panel.add(jtfApellido1);
		jtfApellido1.setColumns(10);
		
		JLabel lblApellidoMaterno = new JLabel("Apellido Materno");
		lblApellidoMaterno.setBounds(27, 135, 133, 15);
		panel.add(lblApellidoMaterno);
		
		jtfApellido2 = new JTextField();
		jtfApellido2.setBounds(178, 133, 114, 19);
		panel.add(jtfApellido2);
		jtfApellido2.setColumns(10);
		
		JLabel lblEdad = new JLabel("Edad");
		lblEdad.setBounds(332, 135, 45, 15);
		panel.add(lblEdad);
		
		jtfEdad = new JTextField();
		jtfEdad.setBounds(407, 133, 114, 19);
		panel.add(jtfEdad);
		jtfEdad.setColumns(10);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 198, 650, 2);
		panel.add(separator);
		
		lblEstudiantes = new JLabel("Estudiantes");
		lblEstudiantes.setBounds(38, 212, 85, 15);
		panel.add(lblEstudiantes);
		
		lblDocentes = new JLabel("Docentes");
		lblDocentes.setBounds(508, 212, 70, 15);
		panel.add(lblDocentes);
		
		lblMatricula = new JLabel("Matrícula");
		lblMatricula.setBounds(27, 274, 70, 15);
		panel.add(lblMatricula);
		
		jtfMatricula = new JTextField();
		jtfMatricula.setBounds(138, 272, 114, 19);
		panel.add(jtfMatricula);
		jtfMatricula.setColumns(10);
		
		lblGeneracion = new JLabel("Generación");
		lblGeneracion.setBounds(27, 348, 96, 15);
		panel.add(lblGeneracion);
		
		jtfGeneracion = new JTextField();
		jtfGeneracion.setBounds(138, 346, 114, 19);
		panel.add(jtfGeneracion);
		jtfGeneracion.setColumns(10);
		
		lblHorario = new JLabel("Horario");
		lblHorario.setBounds(27, 413, 70, 15);
		panel.add(lblHorario);
		
		jtfHorario = new JTextField();
		jtfHorario.setBounds(138, 411, 114, 19);
		panel.add(jtfHorario);
		jtfHorario.setColumns(10);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(0, 498, 650, 2);
		panel.add(separator_1);
		
		jtfCedula = new JTextField();
		jtfCedula.setBounds(508, 272, 114, 19);
		panel.add(jtfCedula);
		jtfCedula.setColumns(10);
		
		jtfEspecialidad = new JTextField();
		jtfEspecialidad.setBounds(508, 346, 114, 19);
		panel.add(jtfEspecialidad);
		jtfEspecialidad.setColumns(10);
		
		jtfCubiculo = new JTextField();
		jtfCubiculo.setBounds(508, 411, 114, 19);
		panel.add(jtfCubiculo);
		jtfCubiculo.setColumns(10);
		
		lblCedula = new JLabel("Cédula");
		lblCedula.setBounds(388, 274, 70, 15);
		panel.add(lblCedula);
		
		lblEspecialidad = new JLabel("Especialidad");
		lblEspecialidad.setBounds(388, 348, 102, 15);
		panel.add(lblEspecialidad);
		
		lblCubculo = new JLabel("Cubículo");
		lblCubculo.setBounds(388, 413, 70, 15);
		panel.add(lblCubculo);
		
		jbtMostrarTodos = new JButton("Mostrar todos");
		jbtMostrarTodos.setBounds(239, 512, 150, 25);
		panel.add(jbtMostrarTodos);
		
		jbtAgregar = new JButton("Agregar");
		jbtAgregar.setBounds(263, 457, 117, 25);
		panel.add(jbtAgregar);
		
		jtaEstudiante = new JList();
		jtaEstudiante.setBounds(30, 540, 280, 150);
		panel.add(jtaEstudiante);
		
		jtaDocente = new JList();
		jtaDocente.setBounds(338, 540, 280, 150);
		panel.add(jtaDocente);
		ocultar();
	}
	public void ocultar() {
		lblGeneracion.setVisible(false);
		jtfGeneracion.setVisible(false);
		lblHorario.setVisible(false);
		jtfHorario.setVisible(false);
		lblMatricula.setVisible(false);
		jtfMatricula.setVisible(false);
		lblCedula.setVisible(false);
		jtfCedula.setVisible(false);
		lblCubculo.setVisible(false);
		jtfCubiculo.setVisible(false);
		lblEspecialidad.setVisible(false);
		jtfEspecialidad.setVisible(false);
		jbtAgregar.setVisible(false);
		
		jtfNombre.setEnabled(false);
		jtfApellido1.setEnabled(false);
		jtfApellido2.setEnabled(false);
		jtfEdad.setEnabled(false);
	}
	
	
	public JList getJtaEstudiante() {
		return jtaEstudiante;
	}

	public void setJtaEstudiante(JList jtaEstudiante) {
		this.jtaEstudiante = jtaEstudiante;
	}

	public void onClickAgregar(ActionListener l) {
		jbtAgregar.addActionListener(l);
	}
	public void onclickMostrar(ActionListener l) {
		jbtMostrarTodos.addActionListener(l);
	}
	public void onItemStateChangeCombo(ItemListener l) {
		comboBox.addItemListener(l);
	}
	public JTextField getJtfNombre() {
		return jtfNombre;
	}

	public void setJtfNombre(JTextField jtfNombre) {
		this.jtfNombre = jtfNombre;
	}

	public JTextField getJtfApellido1() {
		return jtfApellido1;
	}

	public void setJtfApellido1(JTextField jtfApellido1) {
		this.jtfApellido1 = jtfApellido1;
	}

	public JTextField getJtfApellido2() {
		return jtfApellido2;
	}

	public void setJtfApellido2(JTextField jtfApellido2) {
		this.jtfApellido2 = jtfApellido2;
	}

	public JTextField getJtfEdad() {
		return jtfEdad;
	}

	public void setJtfEdad(JTextField jtfEdad) {
		this.jtfEdad = jtfEdad;
	}

	public JTextField getJtfMatricula() {
		return jtfMatricula;
	}

	public void setJtfMatricula(JTextField jtfMatricula) {
		this.jtfMatricula = jtfMatricula;
	}

	public JTextField getJtfGeneracion() {
		return jtfGeneracion;
	}

	public void setJtfGeneracion(JTextField jtfGeneracion) {
		this.jtfGeneracion = jtfGeneracion;
	}

	public JTextField getJtfHorario() {
		return jtfHorario;
	}

	public void setJtfHorario(JTextField jtfHorario) {
		this.jtfHorario = jtfHorario;
	}

	public JTextField getJtfCedula() {
		return jtfCedula;
	}

	public void setJtfCedula(JTextField jtfCedula) {
		this.jtfCedula = jtfCedula;
	}

	public JTextField getJtfEspecialidad() {
		return jtfEspecialidad;
	}

	public void setJtfEspecialidad(JTextField jtfEspecialidad) {
		this.jtfEspecialidad = jtfEspecialidad;
	}

	public JTextField getJtfCubiculo() {
		return jtfCubiculo;
	}

	public void setJtfCubiculo(JTextField jtfCubiculo) {
		this.jtfCubiculo = jtfCubiculo;
	}

	public JComboBox getComboBox() {
		return comboBox;
	}

	public void setComboBox(JComboBox comboBox) {
		this.comboBox = comboBox;
	}

	

	public JButton getJbtMostrarTodos() {
		return jbtMostrarTodos;
	}

	public void setJbtMostrarTodos(JButton jbtMostrarTodos) {
		this.jbtMostrarTodos = jbtMostrarTodos;
	}

	public JButton getJbtAgregar() {
		return jbtAgregar;
	}

	public void setJbtAgregar(JButton jbtAgregar) {
		this.jbtAgregar = jbtAgregar;
	}


	public JLabel getLblMatricula() {
		return lblMatricula;
	}

	public void setLblMatricula(JLabel lblMatricula) {
		this.lblMatricula = lblMatricula;
	}

	public JLabel getLblGeneracion() {
		return lblGeneracion;
	}

	public void setLblGeneracion(JLabel lblGeneracion) {
		this.lblGeneracion = lblGeneracion;
	}

	public JLabel getLblHorario() {
		return lblHorario;
	}

	public void setLblHorario(JLabel lblHorario) {
		this.lblHorario = lblHorario;
	}

	

	public JLabel getLblCedula() {
		return lblCedula;
	}

	public void setLblCedula(JLabel lblCedula) {
		this.lblCedula = lblCedula;
	}

	public JLabel getLblEspecialidad() {
		return lblEspecialidad;
	}

	public void setLblEspecialidad(JLabel lblEspecialidad) {
		this.lblEspecialidad = lblEspecialidad;
	}

	public JLabel getLblCubculo() {
		return lblCubculo;
	}

	public void setLblCubculo(JLabel lblCubculo) {
		this.lblCubculo = lblCubculo;
	}

	public JList getJtaDocente() {
		return jtaDocente;
	}

	public void setJtaDocente(JList jtaDocente) {
		this.jtaDocente = jtaDocente;
	}

	
}
