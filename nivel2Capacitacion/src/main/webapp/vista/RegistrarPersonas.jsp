<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../css/materialize.css" rel="stylesheet">
<title>Registrar Personas</title>
</head>
<body background="fondo.png">
<div class="container">
<div class="row">
<div class="col s11 card-panel" id="frmIngresar">
<i class="material-icons medium" style="cursor:pointer" title="Cerrar sesion"
	onclick="cerrar()">keyboard_return</i>
<h3 class="green-text">Registrar personas</h3>

<div id="frmInsertar">

				<div class="row">
					<div class="input-field col s8">
						<input id="nombre" type="text" class="validate"> <label
							for="nombre" id="la">Nombre</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s8">
						<input id="apellido1" type="text" class="validate"> <label
							for="apellido1">Primer Apelldo</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s8">
						<input id="apellido2" type="text" class="validate"> <label
							for="apellido2">Segundo apellido</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s8">
						<input id="edad" type="text" class="validate"> <label
							for="edad">Edad</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s8">
						<input id="nacionalidad" type="text" class="validate"> <label
							for="nacionalidad">Nacionalidad</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s8">
						<input id="email" type="email" class="validate"> <label
							for="email">Email</label>
						<a class="waves-effect waves-light btn" id="btnInsertar" >Guardar</a>
						<a class="waves-effect waves-light btn" onclick="mostrar('cambio','frmOrdenar');">Consultar</a>
						
					</div>
				</div>
</div>				
				<br> <br>
				<div id="frmOrdenar" style="display:none">
				<div class="input-field col s6">
					<select id="cbOrdenar">
						<option value="" disabled selected>¿Cómo deseas ordenar los registros?</option>
						<option value="primer_apellido">Apellido Paterno</option>
						<option value="nacionalidad">Nacionalidad</option>
						<option value="edad">Edad</option>
					</select> <label>Ordenamiento</label> 
					<a class="waves-effect waves-light btn" onclick="ordenar()">Aceptar</a>
					<a class="waves-effect waves-light btn" onclick="mostrar2('cambio','frmOrdenar')">Regresar</a>
					
				</div>
				<div id="tabla">
					<table class="highlight" id="resultado">
						

						
					</table>
				</div>
				
				</div>
				
</div>
</div>
</div>
</body>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script src="../js/jquery-3.2.1.min.js"></script>
<script src="../js/InsertarConJQ.js" type="text/javascript"></script>
<script src="../js/Registrar.js" type="text/javascript"></script>
<script src="../js/materialize.js" type="text/javascript"></script>
<script src ="../js/init.js"type="text/javascript"></script>

<script>
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems, options);
  });
M.AutoInit();
</script>

  
</html>
