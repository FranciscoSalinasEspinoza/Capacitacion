package com.pearbit.nivel2.modelo;

public class ModeloLogin {
	private String correo,contra;
	private int activa;
	
	
	
	public ModeloLogin() {
		super();
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getContra() {
		return contra;
	}
	public void setContra(String contra) {
		this.contra = contra;
	}
	public int getActiva() {
		return activa;
	}
	public void setActiva(int activa) {
		this.activa = activa;
	}
	
	
}
