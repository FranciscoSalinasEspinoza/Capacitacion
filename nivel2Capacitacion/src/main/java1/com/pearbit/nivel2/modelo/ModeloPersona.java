package com.pearbit.nivel2.modelo;

public class ModeloPersona {
 String nombre,apellido1,apellido2,edad,nacionalidad,correo;

 
 
public ModeloPersona() {
	super();
}

public ModeloPersona(String nombre, String apellido1, String apellido2, String edad, String nacionalidad,
		String correo) {
	super();
	this.nombre = nombre;
	this.apellido1 = apellido1;
	this.apellido2 = apellido2;
	this.edad = edad;
	this.nacionalidad = nacionalidad;
	this.correo = correo;
}

public String getNombre() {
	return nombre;
}

public void setNombre(String nombre) {
	this.nombre = nombre;
}

public String getApellido1() {
	return apellido1;
}

public void setApellido1(String apellido1) {
	this.apellido1 = apellido1;
}

public String getApellido2() {
	return apellido2;
}

public void setApellido2(String apellido2) {
	this.apellido2 = apellido2;
}

public String getEdad() {
	return edad;
}

public void setEdad(String edad) {
	this.edad = edad;
}

public String getNacionalidad() {
	return nacionalidad;
}

public void setNacionalidad(String nacionalidad) {
	this.nacionalidad = nacionalidad;
}

public String getCorreo() {
	return correo;
}

public void setCorreo(String correo) {
	this.correo = correo;
}
 
 
}
