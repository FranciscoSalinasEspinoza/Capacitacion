/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pearbit.nivel2.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author din
 */

/* Design Pattern - Singleton */
public class SingleDBConnection {

    private static SingleDBConnection instance;
    private static String database, user, pass;
    private Connection connection;
    private Statement statement;
    
    private SingleDBConnection() 
            throws ClassNotFoundException {
        SingleDBConnection.database = "nivel5";
        SingleDBConnection.user="root";
        SingleDBConnection.pass="";
        Class.forName("com.mysql.cj.jdbc.Driver");
        
    }
    
    public static SingleDBConnection getInstance(String database ,String user ,String pass ) throws ClassNotFoundException {
        if (instance == null) {
            instance = new SingleDBConnection();
        }
        SingleDBConnection.database =database;
        SingleDBConnection.pass=pass;
        SingleDBConnection.user=user;
        return instance;
    }
    
    public boolean open() {
        String url = "jdbc:mysql://localhost:3306/"+ SingleDBConnection.database+"?serverTimezone=UTC";
        try {
            this.connection = DriverManager.getConnection(url, user, pass);
            this.statement = this.connection.createStatement();
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public ResultSet readData(String query) 
            throws SQLException {
        return this.statement.executeQuery(query);
    }
    
    public void updateData(String sql) 
            throws SQLException {
        this.statement.executeUpdate(sql);
    }

    @Override
    protected void finalize() throws Throwable {
        this.connection.close();
        this.statement.close();
        super.finalize();
    }
    
}
