/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

/**
 *
 * @author ferrioni
 */
import controlador.Controlador;
import vista.Datos;
public class Principal {
    public static void main(String[] args) {
        Datos vista= new Datos();
        Controlador control= new Controlador(vista);
        vista.setVisible(true);
    }
}
