/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import modelo.Modelo;
import vista.Datos;

/**
 *
 * @author ferrioni
 */

public class Controlador implements ActionListener, KeyListener, FocusListener, ItemListener{
private Datos vista;

private Modelo modelo;
private boolean validar1=true;
private boolean validar2=true;
private boolean validar3=true;
private boolean validar4=true;
private boolean validar5=true;
private boolean validar6=true;
int personas;


  public Modelo getModelo() {
	return modelo;
}
public void setModelo(Modelo modelo) {
	this.modelo = modelo;
}
public Controlador() {
	  
  }
    public Controlador(Datos vista) {
        this.vista = vista;
        modelo=new Modelo();
        vista.onClickGuardar(this);
        vista.onKeyPressedEdad((KeyListener) this);
        vista.onKeyPressedNacionalidad((KeyListener) this);
        vista.onFocusLostEmail(this);
        vista.onFocusLostEdad(this);
        vista.onFocusLostAPellidoP(this);
        vista.onFocusLostApellidoM(this);
        vista.onFocusLostNacionalidad(this);
        vista.onFocusLostNombre(this);
        vista.onClickMostrar(this);
        
    }
    @Override
    public void keyPressed(KeyEvent e) {//Metodo para validar cada que se presione una tecla
    if(e.getSource()==vista.getJtfNacionalidad()){
        char digito=e.getKeyChar();
        if(Character.isDigit(digito)){
            vista.MensajeError("No se permiten números", "Campo Nacionalidad");
            vista.getJtfNacionalidad().setText("");
    }
    }
    if(e.getSource()==vista.getJtfEdad()){
        char digito=e.getKeyChar();
        if(Character.isLetter(digito)){
            vista.MensajeError("No se permiten letras", "Campo Edad");
            vista.getJtfEdad().setText("");
    }
    }
    
    }
    @Override
    public void focusLost(FocusEvent e) {
    if(e.getSource()==vista.getJtfCorreo()){//Validación de correo
        String correo=vista.getJtfCorreo().getText();
        Pattern pat = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
       Matcher mat = pat.matcher(correo);
       if(mat.find()){
          validar1=false;
       }else{
          validar1=true; 
          vista.MensajeError("Error", "El correo introducido no es válido");
     }
   
   }
    if(e.getSource()==vista.getJtfNombre()){
        if(!vista.getJtfNombre().getText().isEmpty()){
            validar2=false;
        }
    }if(e.getSource()==vista.getJtfApellido1()){
        if(!vista.getJtfApellido1().getText().isEmpty()){
            validar3=false;
        }
    }if(e.getSource()==vista.getJtfApellido2()){
        if(!vista.getJtfApellido2().getText().isEmpty()){
            validar4=false;
        }
    }if(e.getSource()==vista.getJtfEdad()){
        if(!vista.getJtfEdad().getText().isEmpty()){
            validar5=false;
        }
    }if(e.getSource()==vista.getJtfNacionalidad()){
        if(!vista.getJtfNacionalidad().getText().isEmpty()){
            validar6=false;
        }
    }
    cambiarBtn();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==vista.getJbtGuardar()){
            
            if(vista.getJbtGuardar().getText().equalsIgnoreCase("siguiente >>")){
                modelo.setNombre(vista.getJtfNombre().getText());
                modelo.setApellido1(vista.getJtfApellido1().getText());
                modelo.setApellido2(vista.getJtfApellido2().getText());
                modelo.setCorreo(vista.getJtfCorreo().getText());
                modelo.setEdad(vista.getJtfEdad().getText());
                modelo.setNacionalidad(vista.getJtfNacionalidad().getText());
                limpiar();
                insertarDatos(personas);
                personas++;
                
            }
            else if(vista.getJbtGuardar().getText().equals("Finalizar")){
                cambiarFormulario(modelo.matriz);
            }
          }
        else if(e.getSource()==vista.getJbtMostrar()){
            opciones();
        }
        
    }
    private void insertarDatos(int personas){
         modelo.matriz[personas][0]=modelo.getNombre();
         modelo.matriz[personas][1]=modelo.getApellido1();
         modelo.matriz[personas][2]=modelo.getApellido2();
         modelo.matriz[personas][3]=modelo.getCorreo();
         modelo.matriz[personas][4]=modelo.getEdad();
         modelo.matriz[personas][5]=modelo.getNacionalidad();
         if(personas==2){
             vista.getJtfApellido1().setEnabled(false);
             vista.getJtfApellido2().setEnabled(false);
             vista.getJtfNombre().setEnabled(false);
             vista.getJtfCorreo().setEnabled(false);
             vista.getJtfNacionalidad().setEnabled(false);
             vista.getJtfEdad().setEnabled(false);
             vista.getJbtGuardar().setText("Finalizar");
         }
         cambiarBtn();
         
    }
    public void ordenarApellido(){
        //Este método sirve para ordenar los datos de la matriz en orden alfabético por el apellido
        for (int j=0;j<3;j++){
        for(int i=0;i<2;i++){
        if(modelo.matriz[i][1].compareTo(modelo.matriz[i+1][1])>0){//En essta línea
            //Se compara el dato que está en la posición [i][1], el cual es el 
            //Apelido paterno
            String aux,aux2,aux3,aux4,aux5,aux6;
            aux=modelo.matriz[i][5];
            aux2=modelo.matriz[i][0];
            aux3=modelo.matriz[i][1];
            aux4=modelo.matriz[i][2];
            aux5=modelo.matriz[i][3];
            aux6=modelo.matriz[i][4];
            
            modelo.matriz[i][5]=modelo.matriz[i+1][5];
            modelo.matriz[i][0]=modelo.matriz[i+1][0];
            modelo.matriz[i][1]=modelo.matriz[i+1][1];
            modelo.matriz[i][2]=modelo.matriz[i+1][2];
            modelo.matriz[i][3]=modelo.matriz[i+1][3];
            modelo.matriz[i][4]=modelo.matriz[i+1][4];
            
            modelo.matriz[i+1][5]=aux;
            modelo.matriz[i+1][0]=aux2;
            modelo.matriz[i+1][1]=aux3;
            modelo.matriz[i+1][2]=aux4;
            modelo.matriz[i+1][3]=aux5;
            modelo.matriz[i+1][4]=aux6;
            
            
        }
        }
        
        }
    }
    public void ordenarEdad(){
        for (int j=0;j<3;j++){
        for(int i=0;i<2;i++){
        if(Integer.parseInt(modelo.matriz[i][4])> Integer.parseInt(modelo.matriz[i+1][4])){//En essta línea
            //Se compara el dato que está en la posición [i][4], el cual es la 
            //Edad
            
            String aux,aux2,aux3,aux4,aux5,aux6;
            aux=modelo.matriz[i][5];
            aux2=modelo.matriz[i][0];
            aux3=modelo.matriz[i][1];
            aux4=modelo.matriz[i][2];
            aux5=modelo.matriz[i][3];
            aux6=modelo.matriz[i][4];
            
            modelo.matriz[i][5]=modelo.matriz[i+1][5];
            modelo.matriz[i][0]=modelo.matriz[i+1][0];
            modelo.matriz[i][1]=modelo.matriz[i+1][1];
            modelo.matriz[i][2]=modelo.matriz[i+1][2];
            modelo.matriz[i][3]=modelo.matriz[i+1][3];
            modelo.matriz[i][4]=modelo.matriz[i+1][4];
            
            modelo.matriz[i+1][5]=aux;
            modelo.matriz[i+1][0]=aux2;
            modelo.matriz[i+1][1]=aux3;
            modelo.matriz[i+1][2]=aux4;
            modelo.matriz[i+1][3]=aux5;
            modelo.matriz[i+1][4]=aux6;
            
            
        }
        }
        
        }
    }
    public void ordenarNacionalidad(){
     for (int j=0;j<3;j++){
        for(int i=0;i<2;i++){
        if(modelo.matriz[i][5].compareTo(modelo.matriz[i+1][5])>0){//En essta línea
            //Se compara el dato que está en la posición [i][1], el cual es la 
            //Nacionalidad
            
            String aux,aux2,aux3,aux4,aux5,aux6;
            aux=modelo.matriz[i][5];
            aux2=modelo.matriz[i][0];
            aux3=modelo.matriz[i][1];
            aux4=modelo.matriz[i][2];
            aux5=modelo.matriz[i][3];
            aux6=modelo.matriz[i][4];
            
            modelo.matriz[i][5]=modelo.matriz[i+1][5];
            modelo.matriz[i][0]=modelo.matriz[i+1][0];
            modelo.matriz[i][1]=modelo.matriz[i+1][1];
            modelo.matriz[i][2]=modelo.matriz[i+1][2];
            modelo.matriz[i][3]=modelo.matriz[i+1][3];
            modelo.matriz[i][4]=modelo.matriz[i+1][4];
            
            modelo.matriz[i+1][5]=aux;
            modelo.matriz[i+1][0]=aux2;
            modelo.matriz[i+1][1]=aux3;
            modelo.matriz[i+1][2]=aux4;
            modelo.matriz[i+1][3]=aux5;
            modelo.matriz[i+1][4]=aux6;
            
        }
        }
        
        }  
    }
    private void cambiarBtn(){//Si se cumplen todas las validaciones, el botón cambia 
        //Su texto
        if(!validar1&&!validar2&&!validar3&&!validar4
                &&!validar5&&!validar6){
        vista.getJbtGuardar().setText("Siguiente >>");
    }
    }
    public void mostrar(){
        //Este Método sirve para mostrar los datos después de ser ordenados por alguna
        //de las tres formas requeridas
        String mos="";
        for (int i=0;i<3;i++){
            mos+="\n";
            for(int j=0;j<6;j++){
                mos+=modelo.matriz[i][j]+" "; 
                
                vista.getJtaMostrar().setText(mos+"\n");
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void focusGained(FocusEvent e) {
    
    }

    private void limpiar() {//Este método limpia el formulario después de que se 
        //Ingresa una nueva persona
    vista.getJtfApellido1().setText("");
    vista.getJtfApellido2().setText("");
    vista.getJtfCorreo().setText("");
    vista.getJtfEdad().setText("");
    vista.getJtfNacionalidad().setText("");
    vista.getJtfNombre().setText("");
    validar1=true;
    validar2=true;
    validar3=true;
    validar4=true;
    validar5=true;
    validar6=true;
    vista.getJbtGuardar().setText("Guardar");
    
    
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
       
    }

    public void opciones() {
        //En este metodo se mandan a llamar los diferentes métodos de ordenamiento, dependiendo
        //cual se haya escogido en el formulario
        String opcion=vista.getJcbOpciones().getSelectedItem().toString();
        switch(opcion){
               case "Apellido paterno":
                   ordenarApellido();
                   mostrar();
                   break;
               case "Nacionalidad":
                   ordenarNacionalidad();
                   mostrar();
                   break;
               case "Edad":
                   ordenarEdad();
                   mostrar();
                   break;
               default:
                   vista.getJtaMostrar().setText("");
        }
    }

    private void cambiarFormulario(String[][] matriz) {
    
    	vista.getJbtMostrar().setVisible(true);
        vista.getJtaMostrar().setVisible(true);
        vista.getJcbOpciones().setVisible(true);
    }
        
    
}
