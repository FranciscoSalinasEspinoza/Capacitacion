package com.pearbit.nivel5.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.pearbit.nivel5.control.ControlPersona;
import com.pearbit.nivel5.modelo.ModeloPersona;

/**
 * Servlet implementation class ServletR
 */
@WebServlet("/ServletR")
public class ServletR extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletR() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
//	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=UTF-8");
		try (PrintWriter out = response.getWriter()){
			String nombre=request.getParameter("nombre");
			String apellido1=request.getParameter("apellido1");
			String apellido2=request.getParameter("apellido2");
			String edad=request.getParameter("edad");
			String nacionalidad=request.getParameter("nacionalidad");
			String correo=request.getParameter("correo");
			ModeloPersona objP= new ModeloPersona(nombre, apellido1, apellido2, edad, nacionalidad, correo);
			ControlPersona objC= new ControlPersona(objP);
			out.print(objC.InsertarPersona());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
