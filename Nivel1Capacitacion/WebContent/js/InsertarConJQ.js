/**
 * 
 */
var x;
x=$(document);
x.ready(evento);

/**
 * 
 * Recibe el evennto del click del boton insertar
 * @returns
 */
function evento(){
	var boton=$("#btnInsertar");
	boton.click(obtener);
}

//Obtiene los datos de la vista
function obtener(){
	var nombre=$("#nombre").val();
	var apellido1=$("#apellido1").val();
	var apellido2=$("#apellido2").val();
	var edad=$("#edad").val();
	var nacionalidad=$("#nacionalidad").val();
	var email=$("#email").val();
	var parametros="nombre="+nombre+"&apellido1="+apellido1+"&apellido2="+apellido2
	+"&edad="+edad+"&nacionalidad="+nacionalidad+"&correo="+email;
	//Manda los datos al servlet para la inserccion
	$.ajax({  
	    type: "POST",  
	    url: "../ServletR",  
	    data: parametros,  
	    success: function(result){ 
	    	M.toast({html: 'Persona registrada!! :D'})
	    }                
	  });  
}
