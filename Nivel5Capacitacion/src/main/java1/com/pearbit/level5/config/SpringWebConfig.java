package com.pearbit.level5.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.pearbit.level5.utils.DummyInjection;;
/**
 * 
 * @author Francisco Salinas
 * 
 * This is the class who will give the configuration to Spring, it's very Important!!!
 * @see SpringWebInitializer
 * 
 * Annotations used here are in order for: 
 * {@link Configuration} enable this class to be a configuration file
 * {@link EnableWebMvc} specify that this project will use the WebMVC configuration
 * {@link ComponentScan} allows us scan the mvc components in our class path 
 * {@link PropertySource} it give us the ability to read external files for example, properties files. 
 * {@link Import} this annotations import another configuration classes, in this project will be used to load Security and Session configuration classes.
 * 
 */

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.pearbit.level5"})//I guess this is the route from the application
//will take the resources for working with
@PropertySource("META-INF/application.properties")//This is the source where is the configuration
public class SpringWebConfig extends WebMvcConfigurerAdapter{
	@Value("${database.driver}") private String dbDriver;
	@Value("${database.url}") private String dbUrl;
	@Value("${database.user}") private String dbUser;
	@Value("${database.password}") private String dbPass;
	
	@Value("${hibernate.dialect}") private String hdialect;
	@Value("${hibernate.show_sql}") private String hshowSql;
	@Value("${hibernate.format_sql}") private String hFormatSql;
	@Value("${hibernate.hbm2ddl.auto}") private String ddl_auto;
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").
		addResourceLocations("/WEB-INF/resources/");
         
	}
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver resolver= new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/resources/vista/");
		resolver.setSuffix(".jsp");
		resolver.setExposeContextBeansAsAttributes(true);
		return resolver;
	}
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();  
		sessionFactory.setDataSource(restDataSource());
		sessionFactory.setPackagesToScan(new String[] { "com.pearbit.level5.models" }); 
		sessionFactory.setHibernateProperties(hibernateProperties()); 
		return sessionFactory;
	}
	
	@Bean
	public DataSource restDataSource() {
		BasicDataSource dataSource= new BasicDataSource();
		dataSource.setDriverClassName(dbDriver);
		dataSource.setUrl(dbUrl);
		dataSource.setUsername(dbUser);
		dataSource.setPassword(dbPass);
		return dataSource;
	}
	Properties hibernateProperties() {
		return new Properties() {
			{
			setProperty("hibernate.dialect", hdialect);
			setProperty("hibernate.format_sql", hFormatSql);
			setProperty("hibernate.show_sql", hshowSql);
			setProperty("hibernate.hbm2ddl.auto", ddl_auto);
		}
		};
	}
	

	
}
