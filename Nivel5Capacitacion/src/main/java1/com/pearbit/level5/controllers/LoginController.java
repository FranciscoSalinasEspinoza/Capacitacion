package com.pearbit.level5.controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.pearbit.level5.dao.sq.RolDAO;
import com.pearbit.level5.dao.sq.UserDAO;
import com.pearbit.level5.models.Rol;
import com.pearbit.level5.models.User;

@Controller  //This annotation makes the class work like an controller
@RequestMapping ({"/","/login"})
public class LoginController {

	@Autowired// This annotation creates an automatic reference to UserDAO
	UserDAO daoUser;
	@Autowired
	RolDAO daoRol;
	
	@GetMapping
	public String ShowLoginView() {
		return "Login";
	}
	
	@PostMapping("/perform_login")
	public ModelAndView performLogin(@RequestParam("username")String username,
	@RequestParam("password") String password){
		ModelAndView model= new ModelAndView();
		User user=null;
		try {
			user= daoUser.consultaIndividual(username);
			if(user!=null) {
				if(user.getPassword().equals(password)) {
					model.setViewName("home");
				}else {
					model.setViewName("Login");
					model.addObject("error","Invalid username and password");
				}
			}else {
				model.setViewName("Login");
				model.addObject("error","User does'n exist");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			model.setViewName("Login");
			model.addObject("error",e.getMessage());
		}
		return model;
	}
	
	@GetMapping("/reg")
	public ModelAndView showRegView(String msg) {
		ModelAndView model = new ModelAndView();
		if(msg==null) {
			try {
				List<Rol> roles=daoRol.consultaGeneral();
				if(roles !=null) {
					model.setViewName("register");
					model.addObject("roles",roles);
				
				}else {
					model.setViewName("register");
					model.addObject("error","No roles found");
				}
			}catch (SQLException e) {
				model.setViewName("Login");
				model.addObject("error",e.getMessage());
			}
		}else {
			model.setViewName("register");
			model.addObject("error", msg);
		}
		return model;
	}
	
	@PostMapping("/reg")
	public ModelAndView performRegister(@RequestParam("username")String username,
			@RequestParam("password") String password, @RequestParam("repassword") String repassword,
			@RequestParam("rol") String rol) {
		ModelAndView model = new ModelAndView();
		List<Rol> roles= new ArrayList<>();
		roles.add(new Rol(0,rol));
		try {
			if(password.equals(repassword)) {
				String conf= daoUser.insertar(new User(0,username,password,1,roles));
				if(conf!=""|| conf !=null) {
					model.setViewName("Login");
					model.addObject("msg","User Created!!");
				}else {
					showRegView("The passwords are not the same");
				}
			}
		}catch(SQLException e) {
			e.printStackTrace();
			model.setViewName("Login");
			model.addObject("error",e.getMessage());
		}
		return model;
	}
	
}
