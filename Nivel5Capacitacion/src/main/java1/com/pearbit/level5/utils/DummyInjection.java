package com.pearbit.level5.utils;


import java.sql.SQLException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pearbit.level5.config.SpringWebConfig;
import com.pearbit.level5.dao.sq.RolDAO;
import com.pearbit.level5.models.Rol;
/**
 * This is the easiest example of a Depencency Injection.
 * @author Francisco Salinas
 *
 */
@Component
public class DummyInjection {
	
	@Autowired
	RolDAO dao;
	
	/**
	 * it will be executed after the project construction, after {@link SpringWebConfig}.
	 * @return
	 */
	@PostConstruct
	public boolean createRole(){
		
		Rol rol_adm = new Rol(0, "ADMIN");
		Rol rol_usr = new Rol(0, "USER");

		try {
			dao.insertar(rol_adm);
			dao.insertar(rol_usr);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return true;
	}
	
	
}
