<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"  isELIgnored="false"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


                <!DOCTYPE html>
                <html lang="en">

                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <meta http-equiv="X-UA-Compatible" content="ie=edge">
                    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
                    <link rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/home.css">
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                    <title>Document</title>
                </head>

                <body data-spy="scroll" data-target="#navContainer">

                    <!-- Nav -->
                    <div id="row navContainer">

                        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                            <a href="#" class="navbar-brand">Auctions</a>

                            <!-- Button for menu in small devices  -->
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false"
                                aria-label="Menu de Navegacion">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <!-- Menu content-->
                            <div class="collapse navbar-collapse" id="navbar">

                                <!-- Genral ul with li nesteds MENU OPTIONS-->
                                <ul class="navbar-nav mr-auto fm-nav">
                                    <li class="nav-item">
                                        <a href="#" class="nav-link">Home</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#main" class="nav-link">Blog</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#widgets" class="nav-link">Contact us</a>
                                    </li>
                                    <!-- Dropdown element inside menu -->
                                    <li class="nav-item dropdown">
                                        <a href="#" class="nav-link dropdown-toggle" id="menuMoreCat" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            More
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="menuMoreCat">
                                            <a href="#" class="dropdown-item">More 1</a>
                                            <a href="#" class="dropdown-item">More 3</a>
                                            <a href="#" class="dropdown-item">More 2</a>
                                        </div>

                                    </li>
                                </ul>
                                
                                <!-- Search bar and menu options for loged and not loged users-->
                                <div class="col d-flex flex-column flex-lg-row justify-content-around">
                                    <!-- Search bar -->
                                    <form action="" class="form-inline my-2 my-lg-0 ">
                                        <div class="input-group">
                                            <!-- DROPDOWN CATEGORIES -->
                                            <div class="input-group-btn">
                                                <button class="btn btn-secondary " type="button">
                                                    <span>Filter by</span>
                                                    <span class="caret"></span>
                                                </button>

                                                <button class="btn btn-secondary dropdown-toggle dropdown-toggle-split" id="searchBarCat" data-toggle="dropdown">
                                                    <span class="sr-only btn-secondary">Menu</span>
                                                </button>

                                                <div class="dropdown-menu" aria-labelledby="searchBarCat">
                                                    <a href="#" class="dropdown-item">cat 1</a>
                                                    <a href="#" class="dropdown-item">cat 2</a>
                                                    <div class="dropdown-divider"></div>
                                                    <a href="#" class="dropdown-item">cat 3</a>
                                                    <a href="#" class="dropdown-item">cat 4</a>
                                                </div>
                                            </div>
                                            <input type="hidden" name="search_param" value="all" id="search_param">
                                            <input type="text" class="form-control" name="x" placeholder="Search term...">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button">
                                                    <span class="fa fa-search"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </form>

                                        <div class="btn-group mr-2" role="group" aria-label="First group">
                                            <button class="btn btn-secondary " type="button">
                                                <span>Username</span>
                                                <span class="caret"></span>
                                            </button>
                                            <button class="btn btn-secondary dropdown-toggle dropdown-toggle-split" id="myAccountBtn" data-toggle="dropdown">
                                                <span class="sr-only btn-secondary">Username</span>
                                            </button>
    
                                            <div class="dropdown-menu" aria-labelledby="myAccountBtn">
                                                <a href="#" class="dropdown-item">My Account</a>
                                                <a href="#" class="dropdown-item">Settings</a>
                                                <div class="dropdown-divider"></div>
                                                <a href="<c:url value='/login'/>" class="dropdown-item">Log Out</a>
                                            </div>
                                        </div>                                        
                                    
                                </div>
                            </div>
                        </nav>
                    </div>


                    <!-- Head Container -->
                    <div class="container-fluid full-content">

                        <header class="row">
                            <!-- Carousel -->
                            <div class="col">
                                <div class="carousel slide" id="homeCarousel" data-ride="carousel">
                                    <!-- Buttons for direct access to image -->
                                    <ol class="carousel-indicators">
                                        <li data-target="#homeCarousel" data-slide-to="0" class="active"></li>
                                        <li data-target="#homeCarousel" data-slide-to="1"></li>
                                        <li data-target="#homeCarousel" data-slide-to="2"></li>
                                    </ol>
                                    <!-- Container for elements on carousel -->
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <div class="carousel-caption d-block">
                                                <h4>Title</h4>
                                                <p>Lorem ipsum dolor sit amet.</p>
                                            </div>
                                            <img class="img-fluid rounded" src="<%=request.getContextPath()%>/resources/img/carousel1.jpg" alt="">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="img-fluid rounded" src="<%=request.getContextPath()%>/resources/img/carousel2.jpg" alt="">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="img-fluid rounded" src="<%=request.getContextPath()%>/resources/img/carousel3.jpg" alt="">
                                        </div>
                                    </div>

                                    <a href="#homeCarousel" class="carousel-control-prev" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Anterior</span>
                                    </a>

                                    <a href="#homeCarousel" class="carousel-control-next" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Siguiente</span>
                                    </a>

                                </div>
                            </div>
                        </header>
                    </div>

                    <!-- General Container -->
                    <div class="container">
                        <!-- BODY SECTION -->
                        <section class="container-main row">
                            <!-- MAIN SECTION -->
                            <main class="col-md-8" id="main">
                                <h2>MAIN</h2>
                                <!-- LOGIN MODAL FORM -->
                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h4 class="modal-title">Login</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                <!-- Login Form -->
                                                <form name='f' class="login_form" action="perform_login" method='POST'>
                                                    <!-- Inputs for username -->
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <div class="input-group">
                                                                <span class="input-group-addon fa fa-user fa-1x pt-2 pr-3"></span>
                                                                <input type="text" name="username" id="username" class="form-control" placeholder="Username">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Iputs for Password -->
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <div class="input-group">
                                                                <span class="input-group-addon fa fa-unlock fa-1x pt-2"></span>
                                                                <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- Iputs for csrf -->
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <div class="input-group">
                                                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- Access button -->
                                                    <div class="form-group row">
                                                        <div class="col">
                                                            <button type="submit" class="btn btn-secondary btn-block">Access</button>
                                                            <input type="submit" value="Log In" class="btn btn-secondary btn-block">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                            <!-- Modal footer -->
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam, fugiat facilis excepturi incidunt
                                    sequi quia deserunt reiciendis. Corporis explicabo assumenda eius necessitatibus aperiam
                                    excepturi aliquid temporibus. Quos dignissimos adipisci doloremque!</p>
                                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nemo necessitatibus quo laborum
                                    inventore obcaecati ducimus culpa nisi reprehenderit qui doloribus asperiores, soluta,
                                    eveniet maiores architecto alias exercitationem neque debitis. Atque?</p>
                            </main>
                            <!-- ASIDE SECTION -->
                            <aside class="col-md-4 align-self-center">
                                <h3>ASIDE</h3>
                            </aside>
                        </section>

                        <!-- WIDGEDS SECTION -->
                        <section class="row widgets justify-content-between" id="widgets">
                            <div class="col-12 col-md-4 col-lg-3">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere pariatur quos non odit hic molestiae labore voluptatem harum,
                                vel assumenda corrupti commodi beatae architecto illo porro quia nostrum sequi aperiam!
                                <button class="btn btn-secondary btn-block mt-3 btn-item">
                                    Price: $12.00.
                                </button>
                            </div>
                            <div class="col-12 col-md-4 col-lg-3">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat architecto atque voluptates, aperiam aut sed repellendus
                                qui vel vero nihil cumque at nam eos optio, debitis facilis est quod et!
                            </div>
                            <div class="col-12 col-md-4 col-lg-3">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores cupiditate rerum consectetur, voluptate molestiae nihil
                                vitae, consequuntur est perferendis iusto deserunt alias corporis odio, expedita fugit numquam
                                harum commodi. Beatae.
                            </div>
                        </section>

                        <!-- FOOTER -->
                        <footer class="row">
                            <div class="col">
                                <h4>FOOTER</h4>
                            </div>
                        </footer>

                    </div>


                    <script src="<%=request.getContextPath()%>/resources/js/jquery-3.2.1.js"></script>
                    <script src="<%=request.getContextPath()%>/resources/js/popper.js"></script>
                    <script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
                    <script src="<%=request.getContextPath()%>/resources/js/functions.js"></script>
                    <script>
                        $(document).ready(function () {
                            $(".btn-item").click(function () {
                                window.location.replace('<%=request.getContextPath()%>/item/itemDetail');
                            });
                        });
                    </script>
                    <c:if test="${not empty req}">
                        <script>
                            $('#myModal').modal('show');
                        </script>
                    </c:if>
                </body>

                </html>