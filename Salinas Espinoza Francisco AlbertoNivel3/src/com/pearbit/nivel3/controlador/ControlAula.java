package com.pearbit.nivel3.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.pearbit.nivel3.Modelo.ModeloAula;
import com.pearbit.nivel3.conexion.ConexionBD;
import com.pearbit.nivel3.vista.VistaAula;

public class ControlAula implements ActionListener, MouseListener{
private ModeloAula modeloA;
private VistaAula vistaA;
private ConexionBD conexion;
private ResultSet rs;

public ControlAula(VistaAula vistaA) {
	this.vistaA=vistaA;
	modeloA= new ModeloAula();
	
	vistaA.onclicBtnActualizar(this);
	vistaA.onclicBtnEliminar(this);
	vistaA.onclicBtnGuardar(this);
	vistaA.onMouseClickedTable(this);
}

@Override
public void mouseClicked(MouseEvent e) {
	if(e.getSource()==vistaA.getTable()) {
		seleccionarUno();
	}
	
}

@Override
public void mousePressed(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseReleased(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseEntered(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseExited(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void actionPerformed(ActionEvent e) {
	if(e.getSource()==vistaA.getBtnGuardar()) {
		LlenarModeloAula();
		insertarAula();
	}if(e.getSource()==vistaA.getBtnEliminar()) {
		eliminarAula();
	}if(e.getSource()==vistaA.getBtnActualizar()) {
		actualizar();
	}
	
}

private void LlenarModeloAula() {
	modeloA.setId_aula(vistaA.getTextField().getText());
	modeloA.setTipo(vistaA.getComboBox().getSelectedItem().toString());
	
}

private void insertarAula() {
	String instruccion="insert into aula values ('"+modeloA.getId_aula()
	+"','"+modeloA.getTipo()+"')";
	conexion=new ConexionBD();
	try {
		conexion.insertarRegistro(instruccion);
		conexion.cerrarConexion();
	} catch (Exception e) {
		// TODO: handle exception
	}
	vaciarTabla();
	limpiar();
	
}
public void vaciarTabla() {
	JTable jtaAulas= vistaA.getTable();
	DefaultTableModel modelo= (DefaultTableModel) jtaAulas.getModel();
	int filas=modelo.getRowCount();
	while(filas>1) {
		modelo.removeRow(filas-1);
		filas--;
	}
	llenarTabla();
}
private void llenarTabla() {
	String instruccion="Select*from aula";
	JTable jtaAulas= vistaA.getTable();
	DefaultTableModel modelo= (DefaultTableModel) jtaAulas.getModel();
	conexion=new ConexionBD();
	try {
		rs=conexion.consultarRegistro(instruccion);
		while(rs.next()) {
			int filas=modelo.getRowCount();
			modelo.addRow(new Object[1]);
			modelo.setValueAt(rs.getString(1), filas, 0);
			modelo.setValueAt(rs.getString(2), filas, 1);
			
		}
		conexion.cerrarConexion();
	} catch (Exception e) {
		// TODO: handle exception
	}
}
private void seleccionarUno() {
	JTable jtaAulas= vistaA.getTable();
	DefaultTableModel modelo= (DefaultTableModel) jtaAulas.getModel();
	int fila=jtaAulas.getSelectedRow();
	vistaA.getTextField().setText(modelo.getValueAt(fila, 0).toString());
	vistaA.getComboBox().setSelectedItem(modelo.getValueAt(fila, 1));
	
	vistaA.getBtnActualizar().setEnabled(true);
	vistaA.getBtnEliminar().setEnabled(true);
}
private void eliminarAula() {
	LlenarModeloAula();
	String instruccion="delete from aula where id_aula='"+modeloA.getId_aula()+"'";
	conexion= new ConexionBD();
	try {
		conexion.eliminarRegistro(instruccion);
		conexion.cerrarConexion();
	} catch (Exception e) {
		// TODO: handle exception
	}
	vaciarTabla();
	limpiar();
}
private void actualizar() {
	LlenarModeloAula();
	String instruccion="update aula set id_aula='"+modeloA.getId_aula()+"' "
			+ ",tipo_aula='"+modeloA.getTipo()+"' where id_aula='"+modeloA.getId_aula()+"'";
	conexion= new ConexionBD();
	try {
		conexion.modificar(instruccion);
		conexion.cerrarConexion();
	} catch (Exception e) {
		// TODO: handle exception
	}
	vaciarTabla();
	limpiar();
	
}
private void limpiar() {
	vistaA.getBtnActualizar().setEnabled(false);
	vistaA.getBtnEliminar().setEnabled(false);
	vistaA.getTextField().setText(null);
	vistaA.getComboBox().setSelectedIndex(0);
}
}
