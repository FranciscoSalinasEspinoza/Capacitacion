package com.pearbit.nivel3.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.pearbit.nivel3.conexion.ConexionBD;
import com.pearbit.nivel3.vista.VistaGrupo;
import com.pearbit.nivel3.vista.VistaNuevoGrupo;
import com.pearbit.nivel3.vista.VistaPrincipal;

public class ControlGrupo implements ActionListener, MouseListener{

private VistaGrupo vistaG;
private VistaPrincipal vistaP;
private Control controlP;
private ConexionBD conexion;
private ResultSet rs;

public ControlGrupo(VistaGrupo vistaG) {
	this.vistaG=vistaG;
	vistaP=new VistaPrincipal();
	
	vistaG.onClickBtnAgregarAlumno(this);
	vistaG.onClickBtnAgregarProfesor(this);
	vistaG.onClickBtnEliminarAlumno(this);
	vistaG.onClickBtnEliminarGrupo(this);
	vistaG.onClickBtnEliminarProfesor(this);
	vistaG.onMouseClickedJtableAlumno(this);
	vistaG.onMouseClickedJtableProfesor(this);
	vistaG.onClickBtnSeleccionarGrupo(this);
}



@Override
public void mouseClicked(MouseEvent e) {
	if(e.getSource()==vistaG.getjTableAlumnos()) {
		vistaG.getBtnEliminarAlumno().setEnabled(true);
		
	}if(e.getSource()==vistaG.getjTableProfesores()) {
		vistaG.getBtnEliminarProfesor().setEnabled(true);
	}
	
}

@Override
public void mousePressed(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseReleased(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseEntered(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseExited(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void actionPerformed(ActionEvent e) {
	if(e.getSource()==vistaG.getBtnSeleccionarGrupo()) {
		seleccionarGrupo();
		obtenerAula();
	}if(e.getSource()==vistaG.getBtnAgregarAlumno()) {
		consultarAlumno();
	}if(e.getSource()==vistaG.getBtnEliminarAlumno()) {
		eliminarAlumnos();
	}if(e.getSource()==vistaG.getBtnAgregarProfesor()) {
		consultarDocente();
	}if(e.getSource()==vistaG.getBtnEliminarProfesor()) {
		eliminarDocente();
	}if(e.getSource()==vistaG.getBtnEliminarGrupo()) {
		eliminarGrupo();
	}
}
public void llenarComboGrupos() {
	String instruccion="Select id_grupo from grupo";
	conexion = new ConexionBD();
	try {
		vistaG.getJcbGrupos().removeAllItems();
		rs=conexion.consultarRegistro(instruccion);
		while(rs.next()) {
			vistaG.getJcbGrupos().addItem(rs.getString(1));
		}
		conexion.cerrarConexion();
	} catch (Exception e) {
		// TODO: handle exception
	}
}
private void seleccionarGrupo() {
	String grupo=vistaG.getJcbGrupos().getSelectedItem().toString();
	String instruccion="select id_grupo, nombre_asignatura from grupo inner join asignatura on asignatura.id_asignatura="
			+ "grupo.id_asignatura where id_grupo='"+grupo+"'";
	conexion=new ConexionBD();
	try {
		rs=conexion.consultarRegistro(instruccion);
		while(rs.next()) {
			vistaG.getLblIdGrupo().setText(rs.getString(1));
			vistaG.getLblMateria().setText(rs.getString(2));
		}
		conexion.cerrarConexion();
	} catch (Exception e) {
		// TODO: handle exception
	}
	vaciarTablaAlumnos(grupo);
	
}
private void eliminarGrupo() {
	String grupo=vistaG.getJcbGrupos().getSelectedItem().toString();
	String instruccion="delete from grupo where id_grupo='"+grupo+"'";
	conexion= new ConexionBD();
	try {
		conexion.eliminarRegistro(instruccion);
		conexion.cerrarConexion();
	} catch (Exception e) {
		// TODO: handle exception
	}
	llenarComboGrupos();
	
}
private void vaciarTablaAlumnos(String grupo) {
	JTable jtaAlumnos= vistaG.getjTableAlumnos();
	DefaultTableModel modelo= (DefaultTableModel) jtaAlumnos.getModel();
	int fila=jtaAlumnos.getRowCount();
	while(fila>1) {
		modelo.removeRow(fila-1);
		fila--;
	}
	llenarTablaAlumnos(grupo);
	
}
private void llenarTablaAlumnos(String grupo) {
	System.out.println("Enta al método");
	String instruccion="select alumno.matricula,nombre,apellido_mat,apellido_pat from "
			+ "alumno inner join grupo_alumno on alumno.matricula=grupo_alumno.matricula " + 
			"where grupo_alumno.id_grupo='"+grupo+"'";
	
	JTable jtaAlumnos= vistaG.getjTableAlumnos();
	DefaultTableModel modelo= (DefaultTableModel) jtaAlumnos.getModel();
	conexion=new ConexionBD();
	try {
		rs=new ConexionBD().consultarRegistro(instruccion);
		while (rs.next()) {
			int filas=modelo.getRowCount();
			modelo.addRow(new Object[1]);
			modelo.setValueAt(rs.getString(1), filas, 0);
			modelo.setValueAt(rs.getString(2), filas, 1);
			modelo.setValueAt(rs.getString(3), filas, 2);
			modelo.setValueAt(rs.getString(4), filas, 3);
			
		}
	} catch (Exception e) {
		// TODO: handle exception
	}
	vaciarTablaDocentes(grupo);
}
private void vaciarTablaDocentes(String grupo) {
	JTable jtaDocentes=vistaG.getjTableProfesores();
	DefaultTableModel modelo=(DefaultTableModel) jtaDocentes.getModel();
	int fila=modelo.getRowCount();
	while(fila>1) {
		modelo.removeRow(fila-1);
		fila--;
	}
	
	llenarTablaDocentes(grupo);
}
private void llenarTablaDocentes(String grupo) {
	String instruccion="select profesor.id_profesor,nombre, apellido_pat, apellido_mat from profesor"
			+ " inner join profesor_grupo on profesor.id_profesor=profesor_grupo.id_profesor "
			+ "where id_grupo='"+grupo+"'";
	conexion=new ConexionBD();
	JTable jtaDocentes=vistaG.getjTableProfesores();
	DefaultTableModel modelo=(DefaultTableModel) jtaDocentes.getModel();
	try {
		rs=conexion.consultarRegistro(instruccion);
		while(rs.next()) {
			int filas=modelo.getRowCount();
			modelo.addRow(new Object[1]);
			modelo.setValueAt(rs.getString(1), filas, 0);
			modelo.setValueAt(rs.getString(2), filas, 1);
			modelo.setValueAt(rs.getString(3), filas, 2);
			modelo.setValueAt(rs.getString(4), filas, 3);
			
		}
	} catch (Exception e) {
		// TODO: handle exception
	}
	conexion.cerrarConexion();
}
public void consultarAlumno() {
	String matricula=JOptionPane.showInputDialog(null,"Ingresa la matricula del estudiante");
	String instruccion="select nombre,apellido_pat,apellido_mat from alumno where matricula='"+matricula+"'";
	conexion=new ConexionBD();
	try {
		rs=conexion.consultarRegistro(instruccion);
		while(rs.next()) {
			int mensaje=JOptionPane.showConfirmDialog(null, rs.getString(1)+" "+rs.getString(2)+" "+rs.getString(3)
			+" ","Agregar a este alumno?",JOptionPane.OK_CANCEL_OPTION);
			if(mensaje==0) {
				agregarAlumno(matricula);
			}
		}
	} catch (Exception e) {
		// TODO: handle exception
	}
}
private void agregarAlumno(String matricula) {
	String grupo=vistaG.getJcbGrupos().getSelectedItem().toString();
	String instruccion="insert into grupo_alumno values(null,'"+grupo+"','"+matricula+"')";
	conexion=new ConexionBD();
	try {
		conexion.insertarRegistro(instruccion);
		conexion.cerrarConexion();
	} catch (Exception e) {
		// TODO: handle exception
	}
	vaciarTablaAlumnos(grupo);
	
}
	
private void eliminarAlumnos() {
	String grupo=vistaG.getJcbGrupos().getSelectedItem().toString();
	JTable jtaAlumnos=vistaG.getjTableAlumnos();
	DefaultTableModel modelo=(DefaultTableModel) jtaAlumnos.getModel();
	int fila=jtaAlumnos.getSelectedRow();
	String matricula=modelo.getValueAt(fila, 0).toString();
	String instruccion="delete from grupo_alumno where matricula='"+matricula+"'";
	conexion=new ConexionBD();
	try {
		conexion.eliminarRegistro(instruccion);
		conexion.cerrarConexion();
	} catch (Exception e) {
		// TODO: handle exception
	}
	vaciarTablaAlumnos(grupo);
	regresarBotones();
}
private void consultarDocente() {
	String id=JOptionPane.showInputDialog("Escribe el id del profesor");
	String instruccion="select nombre, apellido_pat,apellido_mat from profesor where id_profesor='"+id+"'";
	conexion=new ConexionBD();
	try {
		rs=conexion.consultarRegistro(instruccion);
		while (rs.next()) {
			int opcion=JOptionPane.showConfirmDialog(null, rs.getString(1)+" "+rs.getString(2)+" "
					+" "+rs.getString(3), "¿Agregar a este Profesor?", JOptionPane.OK_CANCEL_OPTION);
			if(opcion==0) {
				agregarDocente(id);
			}
		}
		conexion.cerrarConexion();
	} catch (Exception e) {
		// TODO: handle exception
	}
}
private void agregarDocente(String id) {
	String grupo=vistaG.getJcbGrupos().getSelectedItem().toString();
	String instruccion="Insert into profesor_grupo values(null,'"+id+"','"+grupo+"')";
	conexion = new ConexionBD();
	try {
		conexion.insertarRegistro(instruccion);
		conexion.cerrarConexion();
	} catch (Exception e) {
		// TODO: handle exception
	}
	vaciarTablaDocentes(grupo);
}
private void eliminarDocente() {
	String grupo=vistaG.getJcbGrupos().getSelectedItem().toString();
	JTable jtaProfesores= vistaG.getjTableProfesores();
	DefaultTableModel modelo=(DefaultTableModel) jtaProfesores.getModel();
	int fila=jtaProfesores.getSelectedRow();
	String id=modelo.getValueAt(fila, 0).toString();
	String instruccion="delete from profesor_grupo where id_profesor='"+id+"'";
	conexion=new ConexionBD();
	try {
		conexion.eliminarRegistro(instruccion);
	} catch (Exception e) {
		// TODO: handle exception
	}
	vaciarTablaAlumnos(grupo);
	regresarBotones();
}
private void obtenerAula() {
	String grupo=vistaG.getJcbGrupos().getSelectedItem().toString();
	String instruccion="select aula.id_aula from aula inner join aula_grupo on aula_grupo.id_aula=aula.id_aula "
			+ "where id_grupo='"+grupo+"'";
	conexion= new ConexionBD();
	try {
		rs=conexion.consultarRegistro(instruccion);
		while(rs.next()) {
			vistaG.getLblAula().setText(rs.getString(1));
			
		}
		conexion.cerrarConexion();
	} catch (Exception e) {
		// TODO: handle exception
	}
}
private void regresarBotones() {
	vistaG.getBtnEliminarAlumno().setEnabled(false);
	vistaG.getBtnEliminarProfesor().setEnabled(false);
}

}
