package com.pearbit.nivel3.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.pearbit.nivel3.Modelo.ModeloDepartamento;
import com.pearbit.nivel3.conexion.ConexionBD;
import com.pearbit.nivel3.vista.VistaDepartamentos;

public class ControlDepartamento implements ActionListener, MouseListener{
private VistaDepartamentos vistaDepartamento;
private ModeloDepartamento modeloDepartamento;
private ConexionBD conexion;


public ControlDepartamento(VistaDepartamentos vistaDepartamento) {
	this.vistaDepartamento=vistaDepartamento;
	modeloDepartamento=new ModeloDepartamento();
	vistaDepartamento.onclickBtnActualizar(this);
	vistaDepartamento.onclickBtnEliminar(this);
	vistaDepartamento.onclickBtnGuardar(this);
	vistaDepartamento.onClickBtnLimpiar(this);
	vistaDepartamento.onMouseClickedTable(this);
}

@Override
public void actionPerformed(ActionEvent e) {
	if(e.getSource()==vistaDepartamento.getJbtGuardar()) {
		if(vistaDepartamento.getJtfNombreDepartamento().getText().isEmpty())
			JOptionPane.showMessageDialog(null, "No puedes dejar el campo nombre vacio", 
					"Error", JOptionPane.ERROR_MESSAGE);
		else {
		llenarModeloDepartamento();
		insertarDepartamento();
		}
	}else if(e.getSource()==vistaDepartamento.getBtnActualizar()) {
		if(vistaDepartamento.getJtfNombreDepartamento().getText().isEmpty())
			JOptionPane.showMessageDialog(null, "No puedes dejar el campo nombre vacio", 
					"Error", JOptionPane.ERROR_MESSAGE);
		else {
			ActualizarDepartamento();
		}
		
	}else if(e.getSource()==vistaDepartamento.getBtnEliminar()) {
		if(vistaDepartamento.getJtfNombreDepartamento().getText().isEmpty())
			JOptionPane.showMessageDialog(null, "No puedes dejar el campo nombre vacio", 
					"Error", JOptionPane.ERROR_MESSAGE);
		else {

			EliminarDepartamento();
		}
	}else if(e.getSource()==vistaDepartamento.getBtnLimpiar()) {
		limpiarFormulario();
	}
	
}


@Override
public void mouseClicked(MouseEvent e) {
	if(e.getSource()==vistaDepartamento.getTable()) {
		seleccionarUno();
	}
	
}


@Override
public void mousePressed(MouseEvent e) {
	// TODO Auto-generated method stub
	
}


@Override
public void mouseReleased(MouseEvent e) {
	// TODO Auto-generated method stub
	
}


@Override
public void mouseEntered(MouseEvent e) {
	// TODO Auto-generated method stub
	
}


@Override
public void mouseExited(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

private void llenarModeloDepartamento() {
	modeloDepartamento=new ModeloDepartamento();
	modeloDepartamento.setDepartamento(vistaDepartamento.getJtfNombreDepartamento().getText());
	
}
//El siguiente método se utiliza para insertar un nuevo departamento en la base de datos
private void insertarDepartamento() {
  final String instruccion="insert into departamento values "
  		+ "(null,'"+modeloDepartamento.getDepartamento()+"')";
  conexion= new ConexionBD();
  try {
	conexion.insertarRegistro(instruccion);
	conexion.cerrarConexion();
} catch (Exception e) {
	
}
  limpiarTabla();
  limpiarFormulario();
}
//Este método sirve para hacer una consulta general de todos los departamentos existentes en la 
//Base de datos
public void consultarTodos() {
	String instruccion="Select*from departamento where id_departamento >1";
	conexion= new ConexionBD();
	JTable jtDepartamento=vistaDepartamento.getTable();
	DefaultTableModel modelo= (DefaultTableModel) jtDepartamento.getModel();
	
	try {
		ResultSet rs=conexion.consultarRegistro(instruccion);
		while(rs.next()) {
			int filas=modelo.getRowCount();
			modelo.addRow(new Object[1]);
			modelo.setValueAt(rs.getString(1), filas, 0);
			modelo.setValueAt(rs.getString(2), filas, 1);
			
		}
	} catch (Exception e) {
		// TODO: handle exception
	}
}
public void limpiarTabla() {
	
	JTable jtDepartamento=vistaDepartamento.getTable();
	DefaultTableModel modelo= (DefaultTableModel) jtDepartamento.getModel();
	int filas=modelo.getRowCount();
	while(filas>1) {
		modelo.removeRow(filas-1);
		filas--;
	}
	consultarTodos();
}

private void seleccionarUno() {
	JTable jtAlumnos=vistaDepartamento.getTable();
	DefaultTableModel modelo= (DefaultTableModel) jtAlumnos.getModel();
	int fila=jtAlumnos.getSelectedRow();
	
	vistaDepartamento.getJtfNombreDepartamento().setText(modelo.getValueAt(fila, 1).toString());
	vistaDepartamento.getJtfIdDepartamento().setText(modelo.getValueAt(fila, 0).toString());
	vistaDepartamento.getBtnActualizar().setEnabled(true);
	vistaDepartamento.getBtnEliminar().setEnabled(true);
	
}

private void ActualizarDepartamento() {
	llenarModeloDepartamento();
	String instruccion="update departamento set nombre_departamento"
			+ "='"+modeloDepartamento.getDepartamento()+"' where id_departamento= "
			+vistaDepartamento.getJtfIdDepartamento().getText();
	conexion=new ConexionBD();
	try {
		conexion.modificar(instruccion);
		conexion.cerrarConexion();
	} catch (Exception e) {
		// TODO: handle exception
	}
	limpiarTabla();
	limpiarFormulario();
}
private void EliminarDepartamento() {
	String Instrucion="Delete from departamento where id_departamento="
			+vistaDepartamento.getJtfIdDepartamento().getText();
	conexion=new ConexionBD();
	try {
		conexion.eliminarRegistro(Instrucion);
		conexion.cerrarConexion();
		
	} catch (Exception e) {
		// TODO: handle exception
	}
	limpiarTabla();
	limpiarFormulario();
}
private void limpiarFormulario() {
	vistaDepartamento.getJtfIdDepartamento().setText("");
	vistaDepartamento.getJtfNombreDepartamento().setText(null);
	vistaDepartamento.getBtnActualizar().setEnabled(false);
	vistaDepartamento.getBtnEliminar().setEnabled(false);
	
}

}
