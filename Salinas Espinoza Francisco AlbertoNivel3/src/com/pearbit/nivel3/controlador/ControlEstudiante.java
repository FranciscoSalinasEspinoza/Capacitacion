package com.pearbit.nivel3.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.pearbit.nivel3.Modelo.ModeloEstudiante;
import com.pearbit.nivel3.conexion.ConexionBD;
import com.pearbit.nivel3.vista.VistaEstudiante;

public class ControlEstudiante implements ActionListener, MouseListener{
private ModeloEstudiante modeloEstudiante;
private VistaEstudiante vistaEstudiante;
private ConexionBD conexion;
private ResultSet rs;

public ControlEstudiante(VistaEstudiante vistaEstudiante) {
	this.vistaEstudiante=vistaEstudiante;
	vistaEstudiante.OnclickBtnRegistrar(this);
	vistaEstudiante.onMouseClickedTable(this);
	vistaEstudiante.onClickEliminar(this);
	vistaEstudiante.onClickLimpiar(this);
	vistaEstudiante.onClickActualizar(this);
}

@Override
public void actionPerformed(ActionEvent e) {
	if(e.getSource()==vistaEstudiante.getBtnRegistrar()) {
		if(!vacios()) {
		llenarModeloEstudiantes();
		registrarEstudiante();
		}else {
			JOptionPane.showMessageDialog(null, "No puedes dejar campos vacíos", 
					"Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	else if(e.getSource()==vistaEstudiante.getBtnEliminar()) {
		if(!vacios()) {
			eliminarEstudiante();
			}else {
				JOptionPane.showMessageDialog(null, "No puedes dejar campos vacíos", 
						"Error", JOptionPane.ERROR_MESSAGE);
			}
	}
	else if(e.getSource()==vistaEstudiante.getBtnLimpiar()) {
		limpiar();
	}
	else if(e.getSource()==vistaEstudiante.getBtnActualizar()) {
		if(!vacios()) {
			actualizarEstudiante();
			}else {
				JOptionPane.showMessageDialog(null, "No puedes dejar campos vacíos", 
						"Error", JOptionPane.ERROR_MESSAGE);
			}
		
	}
}
private void actualizarEstudiante() {
	llenarModeloEstudiantes();
	String instruccion="update alumno set matricula='"
			+modeloEstudiante.getMatricula()
			+"', nombre= '"+modeloEstudiante.getNombre()
			+"', apellido_pat='"+modeloEstudiante.getApellido_pat()
			+"', apellido_mat='"+modeloEstudiante.getApellido_mat()
			+"', fecha_nac='"+modeloEstudiante.getFecha_nac()
			+"' where matricula="+modeloEstudiante.getMatricula();
	conexion= new ConexionBD();
	try {
		conexion.modificar(instruccion);
	} catch (Exception e) {
		// TODO: handle exception
	}
	limpiar();
	vaciarTablaTodos();
}

private void eliminarEstudiante() {
	String instruccion="Delete from alumno where "
			+ "matricula='"+vistaEstudiante.getJtfMatricula().getText()+"'";
	conexion=new ConexionBD();
	try {
		conexion.eliminarRegistro(instruccion);
		conexion.cerrarConexion();
	} catch (Exception e) {
		// TODO: handle exception
	}
	vaciarTablaTodos();
	limpiar();
}

private void llenarModeloEstudiantes() {
	modeloEstudiante=new ModeloEstudiante();
	modeloEstudiante.setApellido_pat(vistaEstudiante.getJtfApellido().getText());
	modeloEstudiante.setApellido_mat(vistaEstudiante.getJtfApellido2().getText());
	modeloEstudiante.setNombre(vistaEstudiante.getJtfNombreAlumno().getText());
	modeloEstudiante.setMatricula(vistaEstudiante.getJtfMatricula().getText());
	modeloEstudiante.setFecha_nac(vistaEstudiante.getJtfFecha_Nac().getText());
	
	
}

private void registrarEstudiante() {
	
	final String instruccion="Insert into alumno values ('"+modeloEstudiante.getMatricula()
	+"','"+modeloEstudiante.getNombre()
	+"','"+modeloEstudiante.getApellido_pat()
	+"','"+modeloEstudiante.getApellido_mat()
	+"','"+modeloEstudiante.getFecha_nac()+"')";
	
	try {
		conexion=new ConexionBD();
		conexion.insertarRegistro(instruccion);
		conexion.cerrarConexion();
		vaciarTablaTodos();
		
	} catch (Exception e) {
		System.out.println(e);
	}
	limpiar();
}
public void consultarTodos() {
	final String instruccion="Select*from alumno";
	try {
		conexion= new ConexionBD();
		rs=conexion.consultarRegistro(instruccion);
		llenarTablaTodos(rs);
		
	} catch (Exception e) {
		// TODO: handle exception
	}
	
}
private void llenarTablaTodos(ResultSet rs) {
	JTable jtAlumnos=vistaEstudiante.getTable();
	DefaultTableModel modelo= (DefaultTableModel) jtAlumnos.getModel();
	
	try {
		while(rs.next()) {
			int filas=modelo.getRowCount();
			modelo.addRow(new Object[1]);
			modelo.setValueAt(rs.getString(1), filas, 0);
			modelo.setValueAt(rs.getString(2), filas, 3);
			modelo.setValueAt(rs.getString(3), filas, 1);
			modelo.setValueAt(rs.getString(4), filas, 2);
			modelo.setValueAt(rs.getString(5), filas, 4);
		
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
}

private void vaciarTablaTodos() {
	JTable jtAlumnos=vistaEstudiante.getTable();
	DefaultTableModel modelo= (DefaultTableModel) jtAlumnos.getModel();
	int filas=modelo.getRowCount();
	while(filas>1) {
		modelo.removeRow(filas-1);
		filas--;
	}
	consultarTodos();
}
private void seleccionarUno() {
	JTable jtAlumnos=vistaEstudiante.getTable();
	DefaultTableModel modelo= (DefaultTableModel) jtAlumnos.getModel();
	int fila=jtAlumnos.getSelectedRow();
	vistaEstudiante.getJtfApellido().setText(modelo.getValueAt(fila, 1).toString());
	vistaEstudiante.getJtfApellido2().setText(modelo.getValueAt(fila, 2).toString());
	vistaEstudiante.getJtfNombreAlumno().setText(modelo.getValueAt(fila, 3).toString());
	vistaEstudiante.getJtfFecha_Nac().setText(modelo.getValueAt(fila, 4).toString());
	vistaEstudiante.getJtfMatricula().setText(modelo.getValueAt(fila, 0).toString());
	vistaEstudiante.getBtnActualizar().setEnabled(true);
	vistaEstudiante.getBtnEliminar().setEnabled(true);
}

@Override
public void mouseClicked(MouseEvent e) {
	if(e.getSource()==vistaEstudiante.getTable()) {
		seleccionarUno();
	}
	
}

@Override
public void mousePressed(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseReleased(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseEntered(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseExited(MouseEvent e) {
	// TODO Auto-generated method stub
	
}
private void limpiar() {
	vistaEstudiante.getJtfApellido().setText("");
	vistaEstudiante.getJtfApellido2().setText("");
	vistaEstudiante.getJtfNombreAlumno().setText("");
	vistaEstudiante.getJtfFecha_Nac().setText("");
	vistaEstudiante.getJtfMatricula().setText("");
	vistaEstudiante.getBtnActualizar().setEnabled(false);
	vistaEstudiante.getBtnEliminar().setEnabled(false);
	
}

public boolean vacios() {
	if(vistaEstudiante.getJtfApellido().getText().isEmpty())
		return true;
	if(vistaEstudiante.getJtfApellido2().getText().isEmpty())
		return true;
	if(vistaEstudiante.getJtfNombreAlumno().getText().isEmpty())
		return true;
	if(vistaEstudiante.getJtfFecha_Nac().getText().isEmpty())
		return true;
	if(vistaEstudiante.getJtfMatricula().getText().isEmpty())
		return true;
	
	
	return false;
}
}
