package com.pearbit.nivel3.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.nio.channels.NetworkChannel;
import java.sql.ResultSet;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.pearbit.nivel3.Modelo.ModeloProfesor;
import com.pearbit.nivel3.conexion.ConexionBD;
import com.pearbit.nivel3.vista.VistaDocente;

public class ControlDocente implements ActionListener,MouseListener{
ModeloProfesor modeloDocente;
VistaDocente vistaDocente;
ConexionBD conexion;

public ControlDocente(VistaDocente vistaDocente) {
	this.vistaDocente=vistaDocente;
	modeloDocente=new ModeloProfesor();
	vistaDocente.onClickBtnActualizar(this);
	vistaDocente.onClickBtnEliminar(this);
	vistaDocente.onClickBtnGuardar(this);
	vistaDocente.onClickBtnLimpiar(this);
	vistaDocente.onMouseClickedTable(this);
	
}

@Override
public void mouseClicked(MouseEvent e) {
	if(e.getSource()==vistaDocente.getTable()) {
		seleccionarUno();
	}
	
}

@Override
public void mousePressed(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseReleased(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseEntered(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void mouseExited(MouseEvent e) {
	// TODO Auto-generated method stub
	
}

@Override
public void actionPerformed(ActionEvent e) {
	if(e.getSource()==vistaDocente.getBtnAgregar()) {
		llenarModeloDocente();
		insertarDocente();
	}else if(e.getSource()==vistaDocente.getBtnEliminar()) {
		eliminarDocente();
	}else if (e.getSource()==vistaDocente.getBtnActualizar()) {
		actualizarDocente();
	}else if(e.getSource()==vistaDocente.getBtnLimpiar()) {
		limpiar();
	}
	
}
private void llenarModeloDocente() {
	modeloDocente.setIdDocente(vistaDocente.getJtfIdDocente().getText());
	modeloDocente.setApellido_pat(vistaDocente.getJtfApellido1().getText());
	modeloDocente.setApellido_mat(vistaDocente.getJtfApellido2().getText());
	modeloDocente.setNombre(vistaDocente.getJtfNombre().getText());
	modeloDocente.setFecha_nac(vistaDocente.getJtfFachaNac().getText());
	modeloDocente.setId_dirige(obtenerIdDirige(vistaDocente.getJcbDirige().getSelectedItem().toString()));;
	modeloDocente.setId_departamento(obtenerIdPertenece(vistaDocente.getJcbPertenece().getSelectedItem().toString()));
	
}

//Este método devuelve el id del departamento que dirige el Docente, si es que dirige uno
private int obtenerIdDirige(String departamento) {
	int idDepartamento=0;
	ResultSet rs;
	if(departamento.equalsIgnoreCase("Ninguno")) {
		return 1;
	}
	else{
		String instruccion="select id_departamento from departamento where "
				+ "nombre_departamento='"+departamento+"'";
		conexion=new ConexionBD();
		try {
		rs=conexion.consultarRegistro(instruccion);
		while(rs.next()) {
			idDepartamento=rs.getInt(1);
		}
		conexion.cerrarConexion();
		
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	return idDepartamento;
}

//Este método devuelve el id del departamento al que pertenece el Docente
private int obtenerIdPertenece(String departamento) {
	int idDepartamento=0;
	ResultSet rs;
	String instruccion="select id_departamento from departamento where "
			+ "nombre_departamento='"+departamento+"'";
	conexion=new ConexionBD();
	try {
	rs=conexion.consultarRegistro(instruccion);
	while(rs.next()) {
		idDepartamento=rs.getInt(1);
	}
	conexion.cerrarConexion();
	
	} catch (Exception e) {
		// TODO: handle exception
	}
	
	return idDepartamento;
}

private void insertarDocente() {
	String instruccion="insert into profesor values ('"
			+modeloDocente.getIdDocente()
			+"','"+modeloDocente.getNombre()
			+"','"+modeloDocente.getApellido_pat()
			+"','"+modeloDocente.getApellido_mat()
			+"','"+modeloDocente.getFecha_nac()
			+"','"+modeloDocente.getId_dirige()
			+"','"+modeloDocente.getId_departamento()
			+"')";
	conexion=new ConexionBD();
	try {
		conexion.insertarRegistro(instruccion);
		conexion.cerrarConexion();
	} catch (Exception e) {
		// TODO: handle exception
	}
	limpiarTabla();
	limpiar();
}
public void llenarCombos() {
	String instruccion="select nombre_departamento from departamento where id_departamento>1";
	conexion =new ConexionBD();
	ResultSet rs;
	try {
		rs=conexion.consultarRegistro(instruccion);
		while(rs.next()) {
			vistaDocente.getJcbDirige().addItem(rs.getString(1));
			vistaDocente.getJcbPertenece().addItem(rs.getString(1));
		}
	} catch (Exception e) {
		// TODO: handle exception
	}
}

public void consultarTodos() {
	JTable jtaDocentes=vistaDocente.getTable();
	DefaultTableModel modelo=(DefaultTableModel) jtaDocentes.getModel();
	String instruccion="Select id_profesor,nombre,apellido_pat, apellido_mat,fecha_nac,"
			+ "departamento.nombre_departamento from" + 
			" profesor inner join departamento on departamento.id_departamento="
			+ "profesor.id_departamento";
	try {
		conexion=new ConexionBD();
		ResultSet rs=conexion.consultarRegistro(instruccion);
		while(rs.next()) {
			int filas=modelo.getRowCount(); 
			modelo.addRow(new Object[1]);
			modelo.setValueAt(rs.getString(1), filas, 0);
			modelo.setValueAt(rs.getString(2), filas, 1);
			modelo.setValueAt(rs.getString(3), filas, 2);
			modelo.setValueAt(rs.getString(4), filas, 3);
			modelo.setValueAt(rs.getString(5), filas, 4);
			modelo.setValueAt(rs.getString(6), filas, 6);
			
		}
	} catch (Exception e) {
		// TODO: handle exception
	}
		consultarDirige();
	}
public void consultarDirige() {
	String instruccion="Select nombre_departamento from  "
			+ "profesor inner join departamento on departamento.id_departamento="
			+ "profesor.id_dirige order by(id_profesor)";
	JTable jtaDocentes=vistaDocente.getTable();
	DefaultTableModel modelo=(DefaultTableModel) jtaDocentes.getModel();
	int fila=1;
	conexion=new ConexionBD();
	try {
		ResultSet rs=conexion.consultarRegistro(instruccion);
		while(rs.next()) {
			modelo.setValueAt(rs.getString(1), fila, 5);
			fila++;
		}
	} catch (Exception e) {
		// TODO: handle exception
	}
}
public void limpiarTabla() {
	JTable jtaDocentes=vistaDocente.getTable();
	DefaultTableModel modelo=(DefaultTableModel) jtaDocentes.getModel();
	int fila=modelo.getRowCount();
	while(fila>1) {
		modelo.removeRow(fila-1);
		fila--;
	}
	consultarTodos();
}
private void seleccionarUno() {
	JTable jtaDocentes=vistaDocente.getTable();
	DefaultTableModel modelo=(DefaultTableModel) jtaDocentes.getModel();
	int fila=jtaDocentes.getSelectedRow();
	vistaDocente.getJtfNombre().setText(modelo.getValueAt(fila, 1).toString());
	vistaDocente.getJtfIdDocente().setText(modelo.getValueAt(fila, 0).toString());
	vistaDocente.getJtfApellido1().setText(modelo.getValueAt(fila, 2).toString());
	vistaDocente.getJtfApellido2().setText(modelo.getValueAt(fila, 3).toString());
	vistaDocente.getJtfFachaNac().setText(modelo.getValueAt(fila, 4).toString());
	vistaDocente.getJcbDirige().setSelectedItem(modelo.getValueAt(fila, 5).toString());
	vistaDocente.getJcbPertenece().setSelectedItem(modelo.getValueAt(fila, 6).toString());
	
	vistaDocente.getBtnActualizar().setEnabled(true);
	vistaDocente.getBtnEliminar().setEnabled(true);
	
}
//Este método se utiliza para eliminar al docente que se elija en la tabla
private void eliminarDocente() {
	llenarModeloDocente();
	String instruccion="delete from profesor where id_profesor='"+modeloDocente.getIdDocente()+"'";
	conexion=new ConexionBD();
	try {
		conexion.eliminarRegistro(instruccion);
		conexion.cerrarConexion();
		limpiarTabla();
		limpiar();
	} catch (Exception e) {
		// TODO: handle exception
	}
}
//Este método funciona para actualizar los datos de algún docente al ser seleccionado en la tabla
private void actualizarDocente() {
	llenarModeloDocente();
	String instruccion="update profesor set nombre='"+modeloDocente.getNombre()
	+"', apellido_pat='"+modeloDocente.getApellido_pat()
	+"', apellido_mat='"+modeloDocente.getApellido_mat()
	+"', fecha_nac='"+modeloDocente.getFecha_nac()
	+"', id_dirige='"+modeloDocente.getId_dirige()
	+"', id_departamento='"+modeloDocente.getId_departamento()
	+"' where id_profesor='"+modeloDocente.getIdDocente()+"'";
	conexion =new ConexionBD();
	try {
		conexion.modificar(instruccion);
		conexion.cerrarConexion();
		limpiarTabla();
		limpiar();
	} catch (Exception e) {
		// TODO: handle exception
	}

}
//Este método sirve para regresar al formulario de los Docentes a su estado inicial
private void limpiar() {
	vistaDocente.getJcbDirige().setSelectedIndex(0);
	vistaDocente.getJcbPertenece().setSelectedIndex(0);
	vistaDocente.getJtfApellido1().setText(null);
	vistaDocente.getJtfApellido2().setText(null);
	vistaDocente.getJtfFachaNac().setText(null);
	vistaDocente.getJtfIdDocente().setText(null);
	vistaDocente.getJtfNombre().setText(null);
	vistaDocente.getBtnActualizar().setEnabled(false);
	vistaDocente.getBtnEliminar().setEnabled(false);
}

}
