package com.pearbit.nivel3.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;

import javax.management.modelmbean.ModelMBeanOperationInfo;
import javax.swing.JMenuItem;

import com.pearbit.nivel3.Modelo.ModeloAsignatura;
import com.pearbit.nivel3.Modelo.ModeloAula;
import com.pearbit.nivel3.Modelo.ModeloDepartamento;
import com.pearbit.nivel3.Modelo.ModeloEstudiante;
import com.pearbit.nivel3.Modelo.ModeloGrupo;
import com.pearbit.nivel3.Modelo.ModeloProfesor;
import com.pearbit.nivel3.conexion.ConexionBD;
import com.pearbit.nivel3.vista.VistaAsignatura;
import com.pearbit.nivel3.vista.VistaAula;
import com.pearbit.nivel3.vista.VistaDepartamentos;
import com.pearbit.nivel3.vista.VistaDocente;
import com.pearbit.nivel3.vista.VistaEstudiante;
import com.pearbit.nivel3.vista.VistaGrupo;
import com.pearbit.nivel3.vista.VistaNuevoGrupo;
import com.pearbit.nivel3.vista.VistaPrincipal;

public class Control implements ActionListener {
private VistaPrincipal vistaP;
private VistaDepartamentos vistaDepartamento;
private VistaDocente vistaDocente;
private VistaAsignatura vistaAsignatura;
private VistaEstudiante vistaEstudiante;
private VistaGrupo vistaGrupo;
private VistaNuevoGrupo vistaNG;
private VistaAula vistaA;

private ControlEstudiante controlEstudiante;
private ControlDepartamento controlDepartamento;
private ControlDocente controlDocente;
private ControlAsignatura controlAsignatura;
private ControlGrupo controlGrupo;
private ControlGrupoNuevo controlNG;
private ControlAula controlA;

//Constructor que inicializa el objeto de la clase principal y agrega los 
//Listeners a la vista principal
public Control(VistaPrincipal vistaP) {
	this.vistaP=vistaP;
    vistaEstudiante=new VistaEstudiante();
    vistaDocente= new VistaDocente();
    vistaDepartamento=new VistaDepartamentos();
    vistaAsignatura=new VistaAsignatura();
    vistaGrupo=new VistaGrupo();
    vistaNG=new VistaNuevoGrupo();
    vistaA= new VistaAula();
    
    vistaP.onClicJmiNuevoGrupo(this);
	vistaP.onClicJmiEstudiante(this);
	vistaP.onCLicJmiDepartamentos(this);
	vistaP.onClicJmiDocente(this);
	vistaP.onClicJmiAsignatura(this);
	vistaP.onClicJmiGrupo(this);
	vistaP.onClicJmiAulas(this);
}

@Override
public void actionPerformed(ActionEvent e) {
	if(e.getSource()==vistaP.getJmiEstudiantes()) {
		mostrarVistaEstudiantes();
	}
	else if(e.getSource()==vistaP.getJmiDepartamentos()) {
		mostrarVistaDepartamento();
	}
	else if(e.getSource()==vistaP.getJmiDocente()) {
		mostrarVistaDocente();
	}
	else if(e.getSource()==vistaP.getJmiAsignatura()) {
		mostrarVistaAsignatura();
	}else if(e.getSource()==vistaP.getJmiGrupos()) {
		mostrarVistaGrupo();
	}else if(e.getSource()==vistaP.getJmiNuevoGrupo()) {
		mostrarVistaNuevoGrupo();
	}else if(e.getSource()==vistaP.getJmiAulas()) {
		mostrarVistaAula();
	}
	
	
}
private void mostrarVistaEstudiantes() {
	vistaP.getContentPane().add(vistaEstudiante);
	vistaEstudiante.setVisible(true);
	controlEstudiante =new ControlEstudiante(vistaEstudiante);
	controlEstudiante.consultarTodos();
}
private void mostrarVistaDepartamento() {
	vistaP.getContentPane().add(vistaDepartamento);
	vistaDepartamento.setVisible(true);
	controlDepartamento= new ControlDepartamento(vistaDepartamento);
	controlDepartamento.consultarTodos();
}
private void mostrarVistaDocente() {
	vistaP.getContentPane().add(vistaDocente);
	vistaDocente.setVisible(true);
	controlDocente= new ControlDocente(vistaDocente);
	controlDocente.llenarCombos();
	controlDocente.consultarTodos();
}

private void mostrarVistaAsignatura() {
	vistaP.getContentPane().add(vistaAsignatura);
	vistaAsignatura.setVisible(true);
	controlAsignatura=new ControlAsignatura(vistaAsignatura);
	controlAsignatura.limpiarTabla();
	
}
private void mostrarVistaGrupo() {
	vistaP.getContentPane().add(vistaGrupo);
	vistaGrupo.setVisible(true);
	controlGrupo=new ControlGrupo(vistaGrupo);
	controlGrupo.llenarComboGrupos();
}

public void mostrarVistaNuevoGrupo() {
	
	vistaP.getContentPane().add(vistaNG);
	vistaNG.setVisible(true);
	controlNG= new ControlGrupoNuevo(vistaNG);
	controlNG.llenarCombo();
	controlNG.llenarComboAulas();
	
}
private void mostrarVistaAula() {
	vistaP.getContentPane().add(vistaA);
	vistaA.setVisible(true);
	controlA= new ControlAula(vistaA);
	controlA.vaciarTabla();
	
}
}
