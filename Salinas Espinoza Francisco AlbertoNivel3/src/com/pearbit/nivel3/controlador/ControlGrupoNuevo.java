package com.pearbit.nivel3.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.channels.NetworkChannel;
import java.sql.ResultSet;

import com.pearbit.nivel3.Modelo.ModeloGrupo;
import com.pearbit.nivel3.conexion.ConexionBD;
import com.pearbit.nivel3.vista.VistaNuevoGrupo;

public class ControlGrupoNuevo implements ActionListener{
	private VistaNuevoGrupo vistaNG;
	private ConexionBD conexion;
	private ResultSet rs;
	private ModeloGrupo modeloG;
	
	public ControlGrupoNuevo(VistaNuevoGrupo vistaNG) {
		super();
		this.vistaNG = vistaNG;
		modeloG=new ModeloGrupo();
		vistaNG.onClicBtnGuardar(this);
		vistaNG.onClicBtnCancelar(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==vistaNG.getBtnAgregar()) {
			llenarModeloNuevoGrupo();
			insertarNuevoGrupo();
		}if(e.getSource()==vistaNG.getBtnCancelar()) {
			vistaNG.dispose();
		}
		
	}
	
	private void llenarModeloNuevoGrupo() {
		modeloG.setId_asignatura(obtenerIdMateria(vistaNG.getJcbMateria().getSelectedItem().toString()));
		modeloG.setId_grupo(vistaNG.getJtfIdGrupo().getText());
		
	}
	private void insertarNuevoGrupo() {
		String instruccion="insert into grupo values ('"+modeloG.getId_grupo()+"','"
				+modeloG.getId_asignatura()+"')";
		conexion=new ConexionBD();
		try {
			conexion.insertarRegistro(instruccion);
			conexion.cerrarConexion();
		} catch (Exception e) {
			// TODO: handle exception
		}
		insertarAula_grupo();
		
	}
	private void insertarAula_grupo() {
		String id_aula=vistaNG.getComboBox().getSelectedItem().toString();
		String id_grupo=vistaNG.getJtfIdGrupo().getText();
		String instruccion="Insert into aula_grupo values (null,'"+id_aula+"','"+id_grupo+"')";
		System.out.println(instruccion);
		conexion= new ConexionBD();
		try {
			conexion.insertarRegistro(instruccion);
			conexion.cerrarConexion();
		} catch (Exception e) {
			// TODO: handle exception
		}
		limpiar();
	}
	public int obtenerIdMateria(String nombreMateria) {
		int id_materia=0;
		String instruccion="Select id_asignatura from asignatura where nombre_asignatura='"
				+nombreMateria+"'";
		conexion=new ConexionBD();
		try {
			rs=conexion.consultarRegistro(instruccion);
			while (rs.next()) {
				id_materia=rs.getInt(1);
			}
			conexion.cerrarConexion();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return id_materia;
	}
	public void llenarCombo() {
		String instruccion="Select nombre_asignatura from asignatura";
		conexion=new ConexionBD();
		try {
			rs=conexion.consultarRegistro(instruccion);
			while(rs.next()) {
				vistaNG.getJcbMateria().addItem(rs.getString(1));
				
			}
			conexion.cerrarConexion();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void llenarComboAulas() {
		String instruccion="select id_aula from aula";
		conexion=new ConexionBD();
		try {
			rs=conexion.consultarRegistro(instruccion);
			while(rs.next()) {
				vistaNG.getComboBox().addItem(rs.getString(1));
			}
			conexion.cerrarConexion();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	public void limpiar() {
		vistaNG.getJtfIdGrupo().setText(null);
		vistaNG.getJcbMateria().setSelectedIndex(0);
	}
	
	
}
