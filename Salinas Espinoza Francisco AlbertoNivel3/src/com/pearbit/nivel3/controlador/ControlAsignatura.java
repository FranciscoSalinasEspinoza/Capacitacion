package com.pearbit.nivel3.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.pearbit.nivel3.Modelo.ModeloAsignatura;
import com.pearbit.nivel3.conexion.ConexionBD;
import com.pearbit.nivel3.vista.VistaAsignatura;

public class ControlAsignatura implements ActionListener, MouseListener{
	private VistaAsignatura vistaAsignatura;
	private ModeloAsignatura modeloAsignatura;
	private ConexionBD conexion;
	private ResultSet rs;
	
	public ControlAsignatura(VistaAsignatura vistaAsignatura) {
		this.vistaAsignatura=vistaAsignatura;
		modeloAsignatura=new ModeloAsignatura();
		
		vistaAsignatura.onclickBtnActualizar(this);
		vistaAsignatura.onclickBtnEliminar(this);
		vistaAsignatura.onclickBtnGuardar(this);
		vistaAsignatura.onclickBtnLimpiar(this);
		vistaAsignatura.onMousClickedTable(this);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if(e.getSource()==vistaAsignatura.getTable()) {
			seleccionarUno();
		}
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==vistaAsignatura.getBtnGuardar()) {
			insertarAsignatura();
		}if(e.getSource()==vistaAsignatura.getBtnActualizar()) {
			actualizarMateria();
		}if(e.getSource()==vistaAsignatura.getBtnEliminar()) {
			eliminarrMateria();
		}if(e.getSource()==vistaAsignatura.getBtnLimpiar()) {
			limpiar();
		}
		
	}
	
	private void llenarModeloAsignatura() {
		modeloAsignatura.setAsignatura(vistaAsignatura.getJtfNombreAsignatura().getText());
	}
	private void insertarAsignatura() {
		llenarModeloAsignatura();
		String instruccion="Insert into asignatura values (null,'"+
		modeloAsignatura.getAsignatura()+"')";
		conexion=new ConexionBD();
		try {
			conexion.insertarRegistro(instruccion);
		} catch (Exception e) {
			// TODO: handle exception
		}
		limpiarTabla();
		
	}
	public void limpiarTabla() {
		JTable jtaAsignatura=vistaAsignatura.getTable();
		DefaultTableModel modelo=(DefaultTableModel)jtaAsignatura.getModel();
		int filas=modelo.getRowCount();
		while(filas>1) {
			modelo.removeRow(filas-1);
			filas--;
		}
		buscarTodos();
	}
	private void buscarTodos() {
		String instruccion="select*from asignatura";
		JTable jtaAsignatura=vistaAsignatura.getTable();
		DefaultTableModel modelo=(DefaultTableModel)jtaAsignatura.getModel();
		conexion=new ConexionBD();
		try {
			rs=conexion.consultarRegistro(instruccion);
			while (rs.next()){
				int filas=modelo.getRowCount();
				modelo.addRow(new Object[1]);
				modelo.setValueAt(rs.getString(1), filas, 0);
				modelo.setValueAt(rs.getString(2), filas, 1);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	
	}
private void seleccionarUno() {
	JTable jtaAsignatura=vistaAsignatura.getTable();
	DefaultTableModel modelo=(DefaultTableModel)jtaAsignatura.getModel();
	int fila=jtaAsignatura.getSelectedRow();
	vistaAsignatura.getJtfIdAsignatura().setText(modelo.getValueAt(fila, 0).toString());
	vistaAsignatura.getJtfNombreAsignatura().setText(modelo.getValueAt(fila, 1).toString());
	vistaAsignatura.getBtnActualizar().setEnabled(true);
	vistaAsignatura.getBtnEliminar().setEnabled(true);
}
private void actualizarMateria() {
	llenarModeloAsignatura();
	String instruccion="update asignatura set nombre_asignatura='"+modeloAsignatura.getAsignatura()
	+"' where id_asignatura="+vistaAsignatura.getJtfIdAsignatura().getText();
	conexion=new ConexionBD();
	try {
		conexion.modificar(instruccion);
	} catch (Exception e) {
		// TODO: handle exception
	}
	limpiarTabla();
	limpiar();
}
private void eliminarrMateria() {
	String instruccion="delete from asignatura where id_asignatura="
	+vistaAsignatura.getJtfIdAsignatura().getText();
	conexion=new ConexionBD();
	try {
		conexion.eliminarRegistro(instruccion);
	} catch (Exception e) {
		// TODO: handle exception
	}
	limpiarTabla();
	limpiar();
}
private void limpiar() {
	vistaAsignatura.getJtfIdAsignatura().setText(null);
	vistaAsignatura.getJtfNombreAsignatura().setText(null);
	vistaAsignatura.getBtnActualizar().setEnabled(false);
	vistaAsignatura.getBtnEliminar().setEnabled(false);
}
}
