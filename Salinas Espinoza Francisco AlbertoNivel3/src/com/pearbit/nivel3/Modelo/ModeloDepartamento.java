package com.pearbit.nivel3.Modelo;

public class ModeloDepartamento {
	String departamento;

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public ModeloDepartamento(String departamento) {
		super();
		this.departamento = departamento;
	}

	public ModeloDepartamento() {
		super();
	}
	
}
