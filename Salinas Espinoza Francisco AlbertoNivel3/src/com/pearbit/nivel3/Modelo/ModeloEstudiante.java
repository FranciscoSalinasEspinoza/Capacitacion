package com.pearbit.nivel3.Modelo;

public class ModeloEstudiante {
	String matricula, nombre, apellido_pat,apellido_mat,fecha_nac;

	public ModeloEstudiante(String matricula, String nombre, String apellido_pat, String apellido_mat,
			String fecha_nac) {
		super();
		this.matricula = matricula;
		this.nombre = nombre;
		this.apellido_pat = apellido_pat;
		this.apellido_mat = apellido_mat;
		this.fecha_nac = fecha_nac;
	}

	public ModeloEstudiante() {
		super();
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido_pat() {
		return apellido_pat;
	}

	public void setApellido_pat(String apellido_pat) {
		this.apellido_pat = apellido_pat;
	}

	public String getApellido_mat() {
		return apellido_mat;
	}

	public void setApellido_mat(String apellido_mat) {
		this.apellido_mat = apellido_mat;
	}

	public String getFecha_nac() {
		return fecha_nac;
	}

	public void setFecha_nac(String fecha_nac) {
		this.fecha_nac = fecha_nac;
	}
	
}
