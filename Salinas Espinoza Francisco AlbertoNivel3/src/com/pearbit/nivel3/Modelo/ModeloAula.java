package com.pearbit.nivel3.Modelo;

public class ModeloAula {
	String tipo;
	String id_aula;
	

	public ModeloAula(String tipo) {
		super();
		this.tipo = tipo;
	}
	

	public String getId_aula() {
		return id_aula;
	}


	public void setId_aula(String id_aula) {
		this.id_aula = id_aula;
	}


	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public ModeloAula() {
		super();
	}
	
}
