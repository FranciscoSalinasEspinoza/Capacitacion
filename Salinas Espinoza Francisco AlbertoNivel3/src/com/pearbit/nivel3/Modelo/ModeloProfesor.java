package com.pearbit.nivel3.Modelo;

public class ModeloProfesor {
	String nombre, apellido_pat,apellido_mat,fecha_nac,idDocente;
	int id_departamento,id_dirige;
	
	public ModeloProfesor(String nombre, String apellido_pat, String apellido_mat, String fecha_nac,
			int id_departamento, int id_dirige) {
		super();
		this.nombre = nombre;
		this.apellido_pat = apellido_pat;
		this.apellido_mat = apellido_mat;
		this.fecha_nac = fecha_nac;
		this.id_departamento = id_departamento;
		this.id_dirige = id_dirige;
	}
	
	
	public String getIdDocente() {
		return idDocente;
	}


	public void setIdDocente(String idDocente) {
		this.idDocente = idDocente;
	}


	public ModeloProfesor() {
		super();
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido_pat() {
		return apellido_pat;
	}
	public void setApellido_pat(String apellido_pat) {
		this.apellido_pat = apellido_pat;
	}
	public String getApellido_mat() {
		return apellido_mat;
	}
	public void setApellido_mat(String apellido_mat) {
		this.apellido_mat = apellido_mat;
	}
	public String getFecha_nac() {
		return fecha_nac;
	}
	public void setFecha_nac(String fecha_nac) {
		this.fecha_nac = fecha_nac;
	}
	public int getId_departamento() {
		return id_departamento;
	}
	public void setId_departamento(int id_departamento) {
		this.id_departamento = id_departamento;
	}
	public int getId_dirige() {
		return id_dirige;
	}
	public void setId_dirige(int id_dirige) {
		this.id_dirige = id_dirige;
	}
	
}
