package com.pearbit.nivel3.Modelo;

public class ModeloGrupo {
	int id_asignatura;
	String id_grupo;
	public ModeloGrupo(int id_asignatura) {
		super();
		this.id_asignatura = id_asignatura;
	}

	public ModeloGrupo() {
		super();
	}
	
	public String getId_grupo() {
		return id_grupo;
	}

	public void setId_grupo(String id_grupo) {
		this.id_grupo = id_grupo;
	}

	public int getId_asignatura() {
		return id_asignatura;
	}

	public void setId_asignatura(int id_asignatura) {
		this.id_asignatura = id_asignatura;
	}
	
}
