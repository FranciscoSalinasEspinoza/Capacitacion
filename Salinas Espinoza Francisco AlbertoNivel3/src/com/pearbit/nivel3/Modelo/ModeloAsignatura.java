package com.pearbit.nivel3.Modelo;

public class ModeloAsignatura{
	String asignatura;
	int idAsignatura;
	public ModeloAsignatura(String asignatura) {
		super();
		this.asignatura = asignatura;
	}

	public String getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(String asignatura) {
		this.asignatura = asignatura;
	}

	public ModeloAsignatura() {
		super();
	}

	public int getIdAsignatura() {
		return idAsignatura;
	}

	public void setIdAsignatura(int idAsignatura) {
		this.idAsignatura = idAsignatura;
	}
	
	
	
}
