package com.pearbit.nivel3.vista;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSeparator;
import javax.swing.JTable;
import java.awt.Label;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import javax.swing.table.DefaultTableModel;

public class VistaDocente extends JInternalFrame {
	private JTextField jtfNombre;
	private JTextField jtfApellido1;
	private JTextField jtfApellido2;
	private JTextField jtfFachaNac;
	private JTable table;
	private JComboBox jcbDirige;
	private JTextField jtfIdDocente;
	private JComboBox jcbPertenece;
	private JButton btnAgregar;
	private JButton btnEliminar;
	private JButton btnActualizar;
	private JButton btnLimpiar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaDocente frame = new VistaDocente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaDocente() {
		setTitle("Gestión de docentes");
		setBounds(0, 0, 950, 850);
		getContentPane().setLayout(null);
		
		JLabel lblRegistrarNuevoDocente = new JLabel("Registrar Nuevo Docente");
		lblRegistrarNuevoDocente.setBounds(12, 12, 194, 15);
		getContentPane().add(lblRegistrarNuevoDocente);
		
		jtfNombre = new JTextField();
		jtfNombre.setBounds(12, 75, 114, 27);
		getContentPane().add(jtfNombre);
		jtfNombre.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(12, 48, 70, 15);
		getContentPane().add(lblNombre);
		
		JLabel lblPrimerApellido = new JLabel("Primer Apellido");
		lblPrimerApellido.setBounds(180, 48, 127, 15);
		getContentPane().add(lblPrimerApellido);
		
		jtfApellido1 = new JTextField();
		jtfApellido1.setBounds(180, 75, 114, 27);
		getContentPane().add(jtfApellido1);
		jtfApellido1.setColumns(10);
		
		JLabel lblSegundoApellido = new JLabel("Segundo Apellido");
		lblSegundoApellido.setBounds(337, 48, 127, 15);
		getContentPane().add(lblSegundoApellido);
		
		jtfApellido2 = new JTextField();
		jtfApellido2.setBounds(337, 75, 114, 27);
		getContentPane().add(jtfApellido2);
		jtfApellido2.setColumns(10);
		
		JLabel lblFechaDeNacimiento = new JLabel("Fecha de nacimiento");
		lblFechaDeNacimiento.setBounds(513, 48, 157, 15);
		getContentPane().add(lblFechaDeNacimiento);
		
		jtfFachaNac = new JTextField();
		jtfFachaNac.setBounds(513, 75, 114, 27);
		getContentPane().add(jtfFachaNac);
		jtfFachaNac.setColumns(10);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(735, 76, 117, 25);
		getContentPane().add(btnAgregar);
		
		btnEliminar = new JButton("ELiminar");
		btnEliminar.setEnabled(false);
		btnEliminar.setBounds(735, 135, 117, 25);
		getContentPane().add(btnEliminar);
		
		btnActualizar = new JButton("Actualizar");
		btnActualizar.setEnabled(false);
		btnActualizar.setBounds(735, 192, 117, 25);
		getContentPane().add(btnActualizar);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.setBounds(735, 241, 117, 25);
		getContentPane().add(btnLimpiar);
		
		JLabel lblDepartamentoAlQue = new JLabel("Departamento al que pertenece");
		lblDepartamentoAlQue.setBounds(190, 126, 237, 15);
		getContentPane().add(lblDepartamentoAlQue);
		
		jcbPertenece = new JComboBox();
		jcbPertenece.setModel(new DefaultComboBoxModel(new String[] {"Seleccionar"}));
		jcbPertenece.setBounds(190, 153, 237, 24);
		getContentPane().add(jcbPertenece);
		
		JLabel lblDepartamentoQueDirige = new JLabel("Departamento que dirige");
		lblDepartamentoQueDirige.setBounds(482, 126, 188, 15);
		getContentPane().add(lblDepartamentoQueDirige);
		
		jcbDirige = new JComboBox();
		jcbDirige.setModel(new DefaultComboBoxModel(new String[] {"Seleccionar", "Ninguno"}));
		jcbDirige.setBounds(478, 153, 192, 24);
		getContentPane().add(jcbDirige);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 300, 950, 2);
		getContentPane().add(separator);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{"Id Profesor", "Nombre", "Apellido P", "Apellido M", "Fecha Nac", "Dirige", "Pertenece"},
			},
			new String[] {
				"Id Profesor", "Nombre", "Apellido P.", "Apellido M.", "Fecha Nac", "Dirige", "Pertenece"
			}
		));
		table.setBounds(61, 340, 800, 300);
		getContentPane().add(table);
		
		JLabel lblIddocente = new JLabel("Id docente");
		lblIddocente.setBounds(12, 126, 100, 15);
		getContentPane().add(lblIddocente);
		
		jtfIdDocente = new JTextField();
		jtfIdDocente.setBounds(12, 152, 114, 27);
		getContentPane().add(jtfIdDocente);
		jtfIdDocente.setColumns(10);

	}
	public void onClickBtnGuardar(ActionListener l) {
		btnAgregar.addActionListener(l);
	}
	public void onClickBtnActualizar(ActionListener l) {
		btnActualizar.addActionListener(l);
	}
	public void onClickBtnLimpiar(ActionListener l) {
		btnLimpiar.addActionListener(l);
	}
	public void onClickBtnEliminar(ActionListener l) {
		btnEliminar.addActionListener(l);
	}
	public void onMouseClickedTable(MouseListener l) {
		table.addMouseListener(l);
	}

	public JTextField getJtfNombre() {
		return jtfNombre;
	}

	public void setJtfNombre(JTextField jtfNombre) {
		this.jtfNombre = jtfNombre;
	}

	public JTextField getJtfApellido1() {
		return jtfApellido1;
	}

	public void setJtfApellido1(JTextField jtfApellido1) {
		this.jtfApellido1 = jtfApellido1;
	}

	public JTextField getJtfApellido2() {
		return jtfApellido2;
	}

	public void setJtfApellido2(JTextField jtfApellido2) {
		this.jtfApellido2 = jtfApellido2;
	}

	public JTextField getJtfFachaNac() {
		return jtfFachaNac;
	}

	public void setJtfFachaNac(JTextField jtfFachaNac) {
		this.jtfFachaNac = jtfFachaNac;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JComboBox getJcbDirige() {
		return jcbDirige;
	}

	public void setJcbDirige(JComboBox jcbDirige) {
		this.jcbDirige = jcbDirige;
	}

	public JTextField getJtfIdDocente() {
		return jtfIdDocente;
	}

	public void setJtfIdDocente(JTextField jtfIdDocente) {
		this.jtfIdDocente = jtfIdDocente;
	}

	public JComboBox getJcbPertenece() {
		return jcbPertenece;
	}

	public void setJcbPertenece(JComboBox jcbPertenece) {
		this.jcbPertenece = jcbPertenece;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public JButton getBtnEliminar() {
		return btnEliminar;
	}

	public void setBtnEliminar(JButton btnEliminar) {
		this.btnEliminar = btnEliminar;
	}

	public JButton getBtnActualizar() {
		return btnActualizar;
	}

	public void setBtnActualizar(JButton btnActualizar) {
		this.btnActualizar = btnActualizar;
	}

	public JButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(JButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}
	
	
}
