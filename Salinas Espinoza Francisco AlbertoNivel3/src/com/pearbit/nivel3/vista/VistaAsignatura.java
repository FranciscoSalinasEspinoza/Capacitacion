package com.pearbit.nivel3.vista;

import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;

public class VistaAsignatura extends JInternalFrame {
	private JTextField jtfNombreAsignatura;
	private JTable table;
	private JButton btnGuardar;
	private JButton btnEliminar;
	private JButton btnActualizar;
	private JButton btnLimpiar;
	private JTextField jtfIdAsignatura;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaAsignatura frame = new VistaAsignatura();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaAsignatura() {
		setTitle("Asignaturas");
		setBounds(0, 0, 950, 750);
		getContentPane().setLayout(null);
		
		JLabel lblNombreDeLa = new JLabel("Nombre de la asignatura");
		lblNombreDeLa.setBounds(47, 35, 183, 15);
		getContentPane().add(lblNombreDeLa);
		
		jtfNombreAsignatura = new JTextField();
		jtfNombreAsignatura.setBounds(47, 62, 142, 41);
		getContentPane().add(jtfNombreAsignatura);
		jtfNombreAsignatura.setColumns(10);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{"Id De las asignatura", "Nombre de la asignatura"},
			},
			new String[] {
				"New column", "New column"
			}
		));
		table.setBounds(440, 75, 450, 500);
		getContentPane().add(table);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(47, 150, 142, 41);
		getContentPane().add(btnGuardar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(47, 224, 142, 41);
		getContentPane().add(btnEliminar);
		
		btnActualizar = new JButton("Actualizar");
		btnActualizar.setBounds(47, 301, 142, 41);
		getContentPane().add(btnActualizar);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.setBounds(47, 374, 147, 41);
		getContentPane().add(btnLimpiar);
		
		jtfIdAsignatura = new JTextField();
		jtfIdAsignatura.setVisible(false);
		jtfIdAsignatura.setEditable(false);
		jtfIdAsignatura.setEnabled(false);
		jtfIdAsignatura.setBounds(47, 472, 114, 19);
		getContentPane().add(jtfIdAsignatura);
		jtfIdAsignatura.setColumns(10);

	}
	
	public void onclickBtnGuardar(ActionListener l) {
		btnGuardar.addActionListener(l);
	}
	public void onclickBtnActualizar(ActionListener l) {
		btnActualizar.addActionListener(l);
	}
	public void onclickBtnEliminar(ActionListener l) {
		btnEliminar.addActionListener(l);
	}
	public void onclickBtnLimpiar(ActionListener l) {
		btnLimpiar.addActionListener(l);
	}
	public void onMousClickedTable(MouseListener l) {
		table.addMouseListener(l);
	}

	public JTextField getJtfNombreAsignatura() {
		return jtfNombreAsignatura;
	}

	public void setJtfNombreAsignatura(JTextField jtfNombreAsignatura) {
		this.jtfNombreAsignatura = jtfNombreAsignatura;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}
	

	public JTextField getJtfIdAsignatura() {
		return jtfIdAsignatura;
	}

	public void setJtfIdAsignatura(JTextField jtfIdAsignatura) {
		this.jtfIdAsignatura = jtfIdAsignatura;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	public JButton getBtnEliminar() {
		return btnEliminar;
	}

	public void setBtnEliminar(JButton btnEliminar) {
		this.btnEliminar = btnEliminar;
	}

	public JButton getBtnActualizar() {
		return btnActualizar;
	}

	public void setBtnActualizar(JButton btnActualizar) {
		this.btnActualizar = btnActualizar;
	}

	public JButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(JButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}
	
	
}
