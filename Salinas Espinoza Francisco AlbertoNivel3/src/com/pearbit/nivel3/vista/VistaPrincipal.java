package com.pearbit.nivel3.vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JDesktopPane;

public class VistaPrincipal extends JFrame {

	private JPanel contentPane;
	private JMenuItem jmiEstudiantes;
	private JDesktopPane desktopPane;
	private JMenuItem jmiDepartamentos;
	private JMenuItem jmiDocente;
	private JMenuItem jmiAsignatura;
	private JMenu jmenuGrupos;
	private JMenuItem jmiGrupos;
	private JMenuItem jmiNuevoGrupo;
	private JMenuItem jmiAulas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaPrincipal frame = new VistaPrincipal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(230, 200, 1000, 800);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu Opciones = new JMenu("Personas");
		menuBar.add(Opciones);
		
		jmiEstudiantes = new JMenuItem("Estudiantes");
		Opciones.add(jmiEstudiantes);
		
		jmiDocente = new JMenuItem("Docentes");
		Opciones.add(jmiDocente);
		
		JMenu menu = new JMenu("Gestion");
		menuBar.add(menu);
		
		jmiAsignatura = new JMenuItem("Asingaturas");
		menu.add(jmiAsignatura);
		
		jmiDepartamentos = new JMenuItem("Departamentos");
		menu.add(jmiDepartamentos);
		
		jmenuGrupos = new JMenu("Grupos");
		menu.add(jmenuGrupos);
		
		jmiGrupos = new JMenuItem("Gestionar grupos");
		jmenuGrupos.add(jmiGrupos);
		
		jmiNuevoGrupo = new JMenuItem("Nuevo Grupo");
		jmenuGrupos.add(jmiNuevoGrupo);
		
		jmiAulas = new JMenuItem("Aulas");
		menu.add(jmiAulas);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		desktopPane = new JDesktopPane();
		contentPane.add(desktopPane, BorderLayout.WEST);
		desktopPane.setLayout(null);
	}
	public void onClicJmiEstudiante(ActionListener l) {
		jmiEstudiantes.addActionListener(l);
	}
	public void onCLicJmiDepartamentos(ActionListener l) {
		jmiDepartamentos.addActionListener(l);
	}
	public void onClicJmiDocente(ActionListener l) {
		jmiDocente.addActionListener(l);
	}
	public void onClicJmiAsignatura(ActionListener l) {
		jmiAsignatura.addActionListener(l);
	}
	public void onClicJmiGrupo(ActionListener l) {
		jmiGrupos.addActionListener(l);
	}
	public void onClicJmiNuevoGrupo(ActionListener l) {
		jmiNuevoGrupo.addActionListener(l);
	}
	public void onClicJmiAulas(ActionListener l) {
		jmiAulas.addActionListener(l);
	}
	
	
	public JMenuItem getJmiAulas() {
		return jmiAulas;
	}

	public void setJmiAulas(JMenuItem jmiAulas) {
		this.jmiAulas = jmiAulas;
	}

	public JMenuItem getJmiEstudiantes() {
		return jmiEstudiantes;
	}

	public void setJmiEstudiantes(JMenuItem jmiEstudiantes) {
		this.jmiEstudiantes = jmiEstudiantes;
	}

	public JDesktopPane getDesktopPane() {
		return desktopPane;
	}

	public void setDesktopPane(JDesktopPane desktopPane) {
		this.desktopPane = desktopPane;
	}

	public JMenuItem getJmiDepartamentos() {
		return jmiDepartamentos;
	}

	public void setJmiDepartamentos(JMenuItem jmiDepartamentos) {
		this.jmiDepartamentos = jmiDepartamentos;
	}

	public JMenuItem getJmiDocente() {
		return jmiDocente;
	}

	public void setJmiDocente(JMenuItem jmiDocente) {
		this.jmiDocente = jmiDocente;
	}

	public JMenuItem getMenuItem() {
		return jmiAsignatura;
	}

	public void setMenuItem(JMenuItem menuItem) {
		this.jmiAsignatura = menuItem;
	}

	public JMenuItem getJmiAsignatura() {
		return jmiAsignatura;
	}

	public void setJmiAsignatura(JMenuItem jmiAsignatura) {
		this.jmiAsignatura = jmiAsignatura;
	}

	public JMenuItem getJmiGrupos() {
		return jmiGrupos;
	}

	public void setJmiGrupos(JMenuItem jmiGrupos) {
		this.jmiGrupos = jmiGrupos;
	}

	public JMenuItem getJmiNuevoGrupo() {
		return jmiNuevoGrupo;
	}

	public void setJmiNuevoGrupo(JMenuItem jmiNuevoGrupo) {
		this.jmiNuevoGrupo = jmiNuevoGrupo;
	}
	
	
	
	
}
