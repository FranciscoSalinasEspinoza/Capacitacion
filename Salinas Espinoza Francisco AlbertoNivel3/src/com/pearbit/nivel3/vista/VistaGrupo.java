package com.pearbit.nivel3.vista;

import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseListener;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;
import java.awt.Font;
import javax.swing.JScrollBar;
import java.awt.event.ActionEvent;

public class VistaGrupo extends JInternalFrame {
	private JTable jTableAlumnos;
	private JTable jTableProfesores;
	private JLabel lblIdGrupo;
	private JLabel lblMateria;
	private JButton btnEliminarProfesor;
	private JButton btnAgregarProfesor;
	private JButton btnAgregarAlumno;
	private JButton btnEliminarAlumno;
	private JComboBox jcbGrupos;
	private JButton btnEliminarGrupo;
	private JButton btnSeleccionarGrupo;
	private JLabel lblAula;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaGrupo frame = new VistaGrupo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaGrupo() {
		getContentPane().setEnabled(false);
		setClosable(true);
		setTitle("Administración de grupos");
		setBounds(0, 0, 1000, 800);
		getContentPane().setLayout(null);
		
		JLabel lblSeleccionarGrupo = new JLabel("Seleccionar grupo");
		lblSeleccionarGrupo.setBounds(12, 12, 153, 15);
		getContentPane().add(lblSeleccionarGrupo);
		
		jcbGrupos = new JComboBox();
		jcbGrupos.setModel(new DefaultComboBoxModel(new String[] {"Seleccionar"}));
		jcbGrupos.setBounds(242, 7, 130, 24);
		getContentPane().add(jcbGrupos);
		
		lblIdGrupo = new JLabel("Id grupo");
		lblIdGrupo.setFont(new Font("Dialog", Font.BOLD, 14));
		lblIdGrupo.setBounds(31, 58, 97, 15);
		getContentPane().add(lblIdGrupo);
		
		lblMateria = new JLabel("Materia ");
		lblMateria.setFont(new Font("Dialog", Font.BOLD, 14));
		lblMateria.setBounds(242, 58, 218, 15);
		getContentPane().add(lblMateria);
		
		btnEliminarGrupo = new JButton("Eliminar grupo");
		btnEliminarGrupo.setBounds(729, 53, 165, 25);
		getContentPane().add(btnEliminarGrupo);
		
		jTableAlumnos = new JTable();
		jTableAlumnos.setModel(new DefaultTableModel(
			new Object[][] {
				{"Matricula", "Nombre", "Apellido Paterno", "Apellido Materno"},
			},
			new String[] {
				"New column", "New column", "New column", "New column"
			}
		));
		jTableAlumnos.getColumnModel().getColumn(2).setPreferredWidth(120);
		jTableAlumnos.getColumnModel().getColumn(3).setPreferredWidth(124);
		jTableAlumnos.setBounds(33, 146, 600, 220);
		getContentPane().add(jTableAlumnos);
		
		jTableProfesores = new JTable();
		jTableProfesores.setModel(new DefaultTableModel(
			new Object[][] {
				{"Id_profesor", "Nombre", "Apellido Paterno", "Apellido materno"},
			},
			new String[] {
				"New column", "New column", "New column", "New column"
			}
		));
		jTableProfesores.getColumnModel().getColumn(2).setPreferredWidth(114);
		jTableProfesores.getColumnModel().getColumn(3).setPreferredWidth(122);
		jTableProfesores.setBounds(33, 440, 600, 220);
		getContentPane().add(jTableProfesores);
		
		JLabel lblAlumnosEnEl = new JLabel("Alumnos en el grupo");
		lblAlumnosEnEl.setBounds(33, 119, 179, 15);
		getContentPane().add(lblAlumnosEnEl);
		
		JLabel lblProfesoresEnEl = new JLabel("Profesores en el grupo");
		lblProfesoresEnEl.setBounds(33, 413, 179, 15);
		getContentPane().add(lblProfesoresEnEl);
		
		btnAgregarAlumno = new JButton("Agregar Alumno");
		btnAgregarAlumno.setBounds(729, 168, 165, 25);
		getContentPane().add(btnAgregarAlumno);
		
		btnEliminarAlumno = new JButton("Eliminar Alumno");
		btnEliminarAlumno.setEnabled(false);
		btnEliminarAlumno.setBounds(729, 264, 165, 25);
		getContentPane().add(btnEliminarAlumno);
		
		btnAgregarProfesor = new JButton("Agregar Profesor");
		btnAgregarProfesor.setBounds(729, 486, 164, 25);
		getContentPane().add(btnAgregarProfesor);
		
		btnEliminarProfesor = new JButton("Eliminar Profesor");
		btnEliminarProfesor.setEnabled(false);
		btnEliminarProfesor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnEliminarProfesor.setBounds(729, 563, 165, 25);
		getContentPane().add(btnEliminarProfesor);
		
		btnSeleccionarGrupo = new JButton("Seleccionar grupo");
		btnSeleccionarGrupo.setBounds(435, 7, 179, 25);
		getContentPane().add(btnSeleccionarGrupo);
		
		lblAula = new JLabel("Aula");
		lblAula.setFont(new Font("Dialog", Font.BOLD, 14));
		lblAula.setBounds(460, 58, 218, 15);
		getContentPane().add(lblAula);

	}
	public void onClickBtnSeleccionarGrupo(ActionListener l) {
		btnSeleccionarGrupo.addActionListener(l);
	}
	
	public void onClickBtnAgregarProfesor(ActionListener l) {
		btnAgregarProfesor.addActionListener(l);
	}
	public void onClickBtnAgregarAlumno(ActionListener l) {
		btnAgregarAlumno.addActionListener(l);
	}
	public void onClickBtnEliminarGrupo(ActionListener l) {
		btnEliminarGrupo.addActionListener(l);
	}
	public void onClickBtnEliminarProfesor(ActionListener l) {
		btnEliminarProfesor.addActionListener(l);
	}
	public void onClickBtnEliminarAlumno(ActionListener l) {
		btnEliminarAlumno.addActionListener(l);
	}
	public void onMouseClickedJtableAlumno(MouseListener l) {
		jTableAlumnos.addMouseListener(l);
	}
	public void onMouseClickedJtableProfesor(MouseListener l) {
		jTableProfesores.addMouseListener(l);
	}
	public JTable getjTableAlumnos() {
		return jTableAlumnos;
	}

	public void setjTableAlumnos(JTable jTableAlumnos) {
		this.jTableAlumnos = jTableAlumnos;
	}

	public JTable getjTableProfesores() {
		return jTableProfesores;
	}

	public void setjTableProfesores(JTable jTableProfesores) {
		this.jTableProfesores = jTableProfesores;
	}

	public JLabel getLblIdGrupo() {
		return lblIdGrupo;
	}

	public void setLblIdGrupo(JLabel lblIdGrupo) {
		this.lblIdGrupo = lblIdGrupo;
	}

	public JLabel getLblMateria() {
		return lblMateria;
	}

	public void setLblMateria(JLabel lblMateria) {
		this.lblMateria = lblMateria;
	}

	public JButton getBtnEliminarProfesor() {
		return btnEliminarProfesor;
	}

	public void setBtnEliminarProfesor(JButton btnEliminarProfesor) {
		this.btnEliminarProfesor = btnEliminarProfesor;
	}

	public JButton getBtnAgregarProfesor() {
		return btnAgregarProfesor;
	}

	public void setBtnAgregarProfesor(JButton btnAgregarProfesor) {
		this.btnAgregarProfesor = btnAgregarProfesor;
	}

	public JButton getBtnAgregarAlumno() {
		return btnAgregarAlumno;
	}

	public void setBtnAgregarAlumno(JButton btnAgregarAlumno) {
		this.btnAgregarAlumno = btnAgregarAlumno;
	}

	public JButton getBtnEliminarAlumno() {
		return btnEliminarAlumno;
	}

	public void setBtnEliminarAlumno(JButton btnEliminarAlumno) {
		this.btnEliminarAlumno = btnEliminarAlumno;
	}

	public JComboBox getJcbGrupos() {
		return jcbGrupos;
	}

	public void setJcbGrupos(JComboBox jcbGrupos) {
		this.jcbGrupos = jcbGrupos;
	}

	

	public JButton getBtnSeleccionarGrupo() {
		return btnSeleccionarGrupo;
	}

	public void setBtnSeleccionarGrupo(JButton btnSeleccionarGrupo) {
		this.btnSeleccionarGrupo = btnSeleccionarGrupo;
	}

	public JButton getBtnEliminarGrupo() {
		return btnEliminarGrupo;
	}

	public void setBtnEliminarGrupo(JButton btnEliminarGrupo) {
		this.btnEliminarGrupo = btnEliminarGrupo;
	}

	public JLabel getLblAula() {
		return lblAula;
	}

	public void setLblAula(JLabel lblAula) {
		this.lblAula = lblAula;
	}
	
}
