package com.pearbit.nivel3.vista;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class VistaEstudiante extends JInternalFrame {
	private JTextField jtfNombreAlumno;
	private JTextField jtfApellido;
	private JTextField jtfApellido2;
	private JTextField jtfMatricula;
	private final JSeparator separator = new JSeparator();
	private JButton btnRegistrar;
	private JTextField jtfFecha_Nac;
	private JTable table;
	private JButton btnEliminar;
	private JButton btnActualizar;
	private JButton btnLimpiar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaEstudiante frame = new VistaEstudiante();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaEstudiante() {
		setNormalBounds(new Rectangle(0, 0, 200, 200));
		setMaximizable(true);
		setClosable(true);
		setTitle("Estudiantes");
		setBounds(0, 0, 950, 750);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblAgregarNuevoEstudiante = new JLabel("Agregar nuevo estudiante");
		lblAgregarNuevoEstudiante.setFont(new Font("Dialog", Font.BOLD, 16));
		lblAgregarNuevoEstudiante.setBounds(50, 23, 301, 37);
		panel.add(lblAgregarNuevoEstudiante);
		
		jtfNombreAlumno = new JTextField();
		jtfNombreAlumno.setBounds(50, 129, 126, 43);
		panel.add(jtfNombreAlumno);
		jtfNombreAlumno.setColumns(10);
		
		jtfApellido = new JTextField();
		jtfApellido.setBounds(296, 129, 126, 43);
		panel.add(jtfApellido);
		jtfApellido.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre del alumno");
		lblNombre.setBounds(50, 97, 153, 15);
		panel.add(lblNombre);
		
		JLabel lblPrimerApellido = new JLabel("Primer Apellido");
		lblPrimerApellido.setBounds(296, 97, 107, 15);
		panel.add(lblPrimerApellido);
		
		jtfApellido2 = new JTextField();
		jtfApellido2.setBounds(526, 129, 126, 43);
		panel.add(jtfApellido2);
		jtfApellido2.setColumns(10);
		
		JLabel lblSegundoApellido = new JLabel("Segundo Apellido");
		lblSegundoApellido.setBounds(526, 97, 126, 15);
		panel.add(lblSegundoApellido);
		
		JLabel lblMatrcula = new JLabel("Matrícula");
		lblMatrcula.setBounds(50, 206, 70, 15);
		panel.add(lblMatrcula);
		
		jtfMatricula = new JTextField();
		jtfMatricula.setBounds(50, 233, 126, 43);
		panel.add(jtfMatricula);
		jtfMatricula.setColumns(10);
		separator.setBounds(0, 314, 950, 2);
		panel.add(separator);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(526, 242, 117, 25);
		panel.add(btnRegistrar);
		
		jtfFecha_Nac = new JTextField();
		jtfFecha_Nac.setBounds(296, 233, 126, 43);
		panel.add(jtfFecha_Nac);
		jtfFecha_Nac.setColumns(10);
		
		JLabel lblFechaDeNacimiento_1 = new JLabel("Fecha de Nacimiento");
		lblFechaDeNacimiento_1.setBounds(296, 206, 169, 15);
		panel.add(lblFechaDeNacimiento_1);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{"Matricula", "Apellido Paterno", "Apellido Materno", "Nombre", "Fecha de nacimiento"},
			},
			new String[] {
				"New column", "New column", "New column", "New column", "New column"
			}
		));
		table.getColumnModel().getColumn(1).setPreferredWidth(127);
		table.getColumnModel().getColumn(2).setPreferredWidth(119);
		table.getColumnModel().getColumn(3).setPreferredWidth(83);
		table.getColumnModel().getColumn(4).setPreferredWidth(146);
		table.setBounds(70, 379, 700, 270);
		panel.add(table);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setEnabled(false);
		btnEliminar.setBounds(771, 122, 117, 25);
		panel.add(btnEliminar);
		
		btnActualizar = new JButton("Actualizar");
		btnActualizar.setEnabled(false);
		btnActualizar.setBounds(771, 180, 117, 25);
		panel.add(btnActualizar);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.setBounds(771, 242, 117, 25);
		panel.add(btnLimpiar);

	}
	
	public void OnclickBtnRegistrar(ActionListener l) {
		btnRegistrar.addActionListener(l);
	}
	
	public void onClickActualizar(ActionListener l) {
		btnActualizar.addActionListener(l);
	}
	
	public void onClickEliminar(ActionListener l) {
		btnEliminar.addActionListener(l);
	}
	
	public void onClickLimpiar(ActionListener l) {
		btnLimpiar.addActionListener(l);
	}

	public JTextField getJtfNombreAlumno() {
		return jtfNombreAlumno;
	}

	public void setJtfNombreAlumno(JTextField jtfNombreAlumno) {
		this.jtfNombreAlumno = jtfNombreAlumno;
	}

	public JTextField getJtfApellido() {
		return jtfApellido;
	}

	public void setJtfApellido(JTextField jtfApellido) {
		this.jtfApellido = jtfApellido;
	}

	public JTextField getJtfApellido2() {
		return jtfApellido2;
	}

	public void setJtfApellido2(JTextField jtfApellido2) {
		this.jtfApellido2 = jtfApellido2;
	}

	
	public JTextField getJtfMatricula() {
		return jtfMatricula;
	}

	public void setJtfMatricula(JTextField jtfMatricula) {
		this.jtfMatricula = jtfMatricula;
	}

	public JButton getBtnRegistrar() {
		return btnRegistrar;
	}

	public void setBtnRegistrar(JButton btnRegistrar) {
		this.btnRegistrar = btnRegistrar;
	}

	public JTextField getJtfFecha_Nac() {
		return jtfFecha_Nac;
	}

	public void setJtfFecha_Nac(JTextField jtfFecha_Nac) {
		this.jtfFecha_Nac = jtfFecha_Nac;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}
	
	public void onMouseClickedTable(MouseListener l) {
		table.addMouseListener(l);
	}

	public JButton getBtnEliminar() {
		return btnEliminar;
	}

	public void setBtnEliminar(JButton btnEliminar) {
		this.btnEliminar = btnEliminar;
	}

	public JButton getBtnActualizar() {
		return btnActualizar;
	}

	public void setBtnActualizar(JButton btnActualizar) {
		this.btnActualizar = btnActualizar;
	}

	public JButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(JButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}
	
}
