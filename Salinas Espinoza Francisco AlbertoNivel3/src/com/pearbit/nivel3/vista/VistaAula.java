package com.pearbit.nivel3.vista;

import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;

public class VistaAula extends JInternalFrame {
	private JTextField jtfIdMateria;
	private JTable table;
	private JComboBox comboBox;
	private JButton btnGuardar;
	private JButton btnEliminar;
	private JButton btnActualizar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaAula frame = new VistaAula();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaAula() {
		setTitle("Aulas");
		setBounds(100, 100, 750, 800);
		getContentPane().setLayout(null);
		
		JLabel lblIdAula = new JLabel("Id Aula");
		lblIdAula.setBounds(33, 41, 70, 15);
		getContentPane().add(lblIdAula);
		
		JLabel lblTipoDeAula = new JLabel("Tipo de aula");
		lblTipoDeAula.setBounds(33, 137, 100, 15);
		getContentPane().add(lblTipoDeAula);
		
		jtfIdMateria = new JTextField();
		jtfIdMateria.setBounds(366, 30, 114, 38);
		getContentPane().add(jtfIdMateria);
		jtfIdMateria.setColumns(10);
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Seleccionar", "Clases", "Auditorio", "Laboratorio C", "Laboratorio Q"}));
		comboBox.setBounds(313, 125, 167, 38);
		getContentPane().add(comboBox);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(33, 187, 117, 25);
		getContentPane().add(btnGuardar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setEnabled(false);
		btnEliminar.setBounds(280, 187, 117, 25);
		getContentPane().add(btnEliminar);
		
		btnActualizar = new JButton("Actualizar");
		btnActualizar.setEnabled(false);
		btnActualizar.setBounds(539, 187, 117, 25);
		getContentPane().add(btnActualizar);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{"Id Aula", "Tipo de Aula"},
			},
			new String[] {
				"New column", "New column"
			}
		));
		table.getColumnModel().getColumn(1).setPreferredWidth(109);
		table.setBounds(68, 287, 600, 350);
		getContentPane().add(table);

	}
	
	public void onclicBtnGuardar(ActionListener l) {
		btnGuardar.addActionListener(l);
	}
	public void onclicBtnActualizar(ActionListener l) {
		btnActualizar.addActionListener(l);
	}
	public void onclicBtnEliminar(ActionListener l) {
		btnEliminar.addActionListener(l);
	}
	public void onMouseClickedTable(MouseListener l) {
		table.addMouseListener(l);
	}
	

	public JTextField getTextField() {
		return jtfIdMateria;
	}

	public void setTextField(JTextField textField) {
		this.jtfIdMateria = textField;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JComboBox getComboBox() {
		return comboBox;
	}

	public void setComboBox(JComboBox comboBox) {
		this.comboBox = comboBox;
	}

	public JButton getBtnGuardar() {
		return btnGuardar;
	}

	public void setBtnGuardar(JButton btnGuardar) {
		this.btnGuardar = btnGuardar;
	}

	public JButton getBtnEliminar() {
		return btnEliminar;
	}

	public void setBtnEliminar(JButton btnEliminar) {
		this.btnEliminar = btnEliminar;
	}

	public JButton getBtnActualizar() {
		return btnActualizar;
	}

	public void setBtnActualizar(JButton btnActualizar) {
		this.btnActualizar = btnActualizar;
	}
	
	
}
