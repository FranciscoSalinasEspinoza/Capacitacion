package com.pearbit.nivel3.vista;

import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.JInternalFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

public class VistaDepartamentos extends JInternalFrame {
	private JTextField jtfNombreDepartamento;
	private JTable table;
	private JButton btnLimpiar;
	private JButton btnActualizar;
	private JButton btnEliminar;
	private JButton jbtGuardar;
	private JTextField jtfIdDepartamento;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaDepartamentos frame = new VistaDepartamentos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaDepartamentos() {
		setTitle("Gestión de departamentos");
		setIconifiable(true);
		setMaximizable(true);
		setClosable(true);
		setBounds(0, 0, 650,450);
		getContentPane().setLayout(null);
		
		jtfNombreDepartamento = new JTextField();
		jtfNombreDepartamento.setBounds(57, 62, 154, 51);
		getContentPane().add(jtfNombreDepartamento);
		jtfNombreDepartamento.setColumns(10);
		
		JLabel lblNombreDelDepartamento = new JLabel("Nombre del Departamento");
		lblNombreDelDepartamento.setBounds(57, 35, 211, 15);
		getContentPane().add(lblNombreDelDepartamento);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{"ID del departamento", "Nombre Del departamento"},
			},
			new String[] {
				"New column", "New column"
			}
		));
		table.getColumnModel().getColumn(0).setPreferredWidth(141);
		table.getColumnModel().getColumn(1).setPreferredWidth(158);
		table.setBounds(302, 49, 300, 300);
		getContentPane().add(table);
		
		jbtGuardar = new JButton("Guardar");
		jbtGuardar.setBounds(57, 135, 117, 25);
		getContentPane().add(jbtGuardar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setEnabled(false);
		btnEliminar.setBounds(57, 205, 117, 25);
		getContentPane().add(btnEliminar);
		
		btnActualizar = new JButton("Actualizar");
		btnActualizar.setEnabled(false);
		btnActualizar.setBounds(57, 272, 117, 25);
		getContentPane().add(btnActualizar);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.setBounds(57, 332, 117, 25);
		getContentPane().add(btnLimpiar);
		
		jtfIdDepartamento = new JTextField();
		jtfIdDepartamento.setOpaque(false);
		jtfIdDepartamento.setFocusable(false);
		jtfIdDepartamento.setVisible(false);
		jtfIdDepartamento.setEnabled(false);
		jtfIdDepartamento.setEditable(false);
		jtfIdDepartamento.setBounds(57, 399, 114, 19);
		getContentPane().add(jtfIdDepartamento);
		jtfIdDepartamento.setColumns(10);

	}
	
	public void onclickBtnGuardar(ActionListener l) {
		jbtGuardar.addActionListener(l);
	}
	public void onclickBtnEliminar(ActionListener l) {
		btnEliminar.addActionListener(l);
	}
	public void onclickBtnActualizar(ActionListener l) {
		btnActualizar.addActionListener(l);
	}
	public void onClickBtnLimpiar(ActionListener l) {
		btnLimpiar.addActionListener(l);
	}
	public void onMouseClickedTable(MouseListener l) {
		table.addMouseListener(l);
	}
	

	public JTextField getJtfNombreDepartamento() {
		return jtfNombreDepartamento;
	}

	public void setJtfNombreDepartamento(JTextField jtfNombreDepartamento) {
		this.jtfNombreDepartamento = jtfNombreDepartamento;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JButton getBtnLimpiar() {
		return btnLimpiar;
	}

	public void setBtnLimpiar(JButton btnLimpiar) {
		this.btnLimpiar = btnLimpiar;
	}

	public JButton getBtnActualizar() {
		return btnActualizar;
	}

	public void setBtnActualizar(JButton btnActualizar) {
		this.btnActualizar = btnActualizar;
	}

	public JButton getBtnEliminar() {
		return btnEliminar;
	}

	public void setBtnEliminar(JButton btnEliminar) {
		this.btnEliminar = btnEliminar;
	}

	public JButton getJbtGuardar() {
		return jbtGuardar;
	}

	public void setJbtGuardar(JButton jbtGuardar) {
		this.jbtGuardar = jbtGuardar;
	}

	public JTextField getJtfIdDepartamento() {
		return jtfIdDepartamento;
	}

	public void setJtfIdDepartamento(JTextField jtfIdDepartamento) {
		this.jtfIdDepartamento = jtfIdDepartamento;
	}
	
}
