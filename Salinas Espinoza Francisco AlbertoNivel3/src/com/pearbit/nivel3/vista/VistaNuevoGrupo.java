package com.pearbit.nivel3.vista;

import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JInternalFrame;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

public class VistaNuevoGrupo extends JInternalFrame {
	private JTextField jtfIdGrupo;
	private JButton btnAgregar;
	private JButton btnCancelar;
	private JComboBox jcbMateria;
	private JComboBox comboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaNuevoGrupo frame = new VistaNuevoGrupo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaNuevoGrupo() {
		setMaximumSize(new Dimension(500, 600));
		setClosable(true);
		setMaximizable(true);
		setIconifiable(true);
		setResizable(true);
		setTitle("Crear un nuevo Grupo");
		setBounds(100, 100, 500, 600);
		getContentPane().setLayout(null);
		
		JLabel lblIdDelGrupo = new JLabel("Id del grupo:");
		lblIdDelGrupo.setBounds(40, 30, 125, 15);
		getContentPane().add(lblIdDelGrupo);
		
		jtfIdGrupo = new JTextField();
		jtfIdGrupo.setBounds(317, 19, 114, 38);
		getContentPane().add(jtfIdGrupo);
		jtfIdGrupo.setColumns(10);
		
		JLabel lblMateria = new JLabel("Materia");
		lblMateria.setBounds(40, 119, 70, 15);
		getContentPane().add(lblMateria);
		
		jcbMateria = new JComboBox();
		jcbMateria.setModel(new DefaultComboBoxModel(new String[] {"Seleccionar"}));
		jcbMateria.setBounds(264, 107, 171, 38);
		getContentPane().add(jcbMateria);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(48, 288, 117, 38);
		getContentPane().add(btnCancelar);
		
		btnAgregar = new JButton("Agregar  grupo");
		btnAgregar.setBounds(264, 288, 167, 38);
		getContentPane().add(btnAgregar);
		
		JLabel lblAula = new JLabel("Aula");
		lblAula.setBounds(40, 193, 70, 15);
		getContentPane().add(lblAula);
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Seleccionar"}));
		comboBox.setBounds(264, 188, 167, 38);
		getContentPane().add(comboBox);

	}
	public void onClicBtnGuardar(ActionListener l) {
		btnAgregar.addActionListener(l);
	}
	public void onClicBtnCancelar(ActionListener l) {
		btnCancelar.addActionListener(l);
	}

	public JTextField getJtfIdGrupo() {
		return jtfIdGrupo;
	}

	public void setJtfIdGrupo(JTextField jtfIdGrupo) {
		this.jtfIdGrupo = jtfIdGrupo;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public JComboBox getJcbMateria() {
		return jcbMateria;
	}

	public void setJcbMateria(JComboBox jcbMateria) {
		this.jcbMateria = jcbMateria;
	}

	public JComboBox getComboBox() {
		return comboBox;
	}

	public void setComboBox(JComboBox comboBox) {
		this.comboBox = comboBox;
	}
	
}
