package com.pearbit.nivel4.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.pearbit.nivel4.conexion.ConexionBD;
import com.pearbit.nivel4.modelo.ModeloPersona;
import com.pearbit.nivel4.vista.VistaLlamada;
import com.pearbit.nivel4.vista.VistaPerso;

public class ControlPersona implements ActionListener {

	private VistaPerso vistaP;
	private ModeloPersona modeloP;
	private ControlLlamada controlL;
	private VistaLlamada vistaL;
	private ConexionBD conexion;

	public ControlPersona(VistaPerso vistaP) {
		this.vistaP = vistaP;
		modeloP = new ModeloPersona();
		vistaP.onClickbtnGuardar(this);
		vistaP.onClickBtnLlamar(this);;

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == vistaP.getBtnAgregar()) {
			llenarModeloP();
		}

	}

	private void llenarModeloP() {
		// Este método llena el modelo de personas antes de insertar en la base de datos
		modeloP.setNombre(vistaP.getJtfNombre().getText());
		modeloP.setApellido_paterno(vistaP.getJtfApaterno().getText());
		modeloP.setApellido_materno(vistaP.getJtfAmaterno().getText());
		modeloP.setEdad(Integer.parseInt(vistaP.getJtfEdad().getText()));
		modeloP.setSexo(vistaP.getJcbSexo().getSelectedItem().toString());
		modeloP.setTelefono(vistaP.getJtfTelefono().getText());
		modeloP.setCodigo(Integer.parseInt(vistaP.getJtfCodigo().getText()));
		modeloP.setTelefonia(vistaP.getJtfTelefonia().toString());
		guardarTelefono();
	}

	private void guardarTelefono() {
		// Éste método inserta los datos del teléfono de la persona
		String instruccion = "insert into telefono values ('" + modeloP.getTelefono() + "','" + modeloP.getCodigo()
				+ "','" + modeloP.getTelefonia() + "')";
		conexion = new ConexionBD();
		try {
			conexion.insertarRegistro(instruccion);
			conexion.cerrarConexion();
		} catch (Exception e) {
			// TODO: handle exception
		}
		guardarPersona();
	}

	private void guardarPersona() {
		String instruccion = "Insert into persona values (null,'" + modeloP.getNombre() 
		+ "','"+ modeloP.getApellido_paterno() + "','" + modeloP.getApellido_materno() 
		+ "'," + modeloP.getEdad()
		+ ",'" + modeloP.getSexo() 
		+ "','" + modeloP.getTelefono() + "')";
		conexion = new ConexionBD();
		try {
			conexion.insertarRegistro(instruccion);
			conexion.cerrarConexion();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
