package com.pearbit.nivel4.conexion;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

public class ConexionBD {
    Connection conn;
    private String usuario;
    private String passw;
    private String nombreBD;
    private String url;
    
    public ConexionBD(){
    nombreBD="nivel4";
    usuario="root";
    passw="";
    url="jdbc:mysql://localhost/"+nombreBD;
    obtenerConexion();
    }
public static void main(String[]args){
    new ConexionBD();
}
    private void obtenerConexion() {
        try {
            conn=DriverManager.getConnection(url,usuario,passw);
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            //JOptionPane.showMessageDialog(null, "Correctamente conectado","ok",JOptionPane.INFORMATION_MESSAGE);
            
            }
            
          catch (InstantiationException ex) {
            JOptionPane.showMessageDialog(null, "No se pudo crear la instancia","Error",JOptionPane.ERROR_MESSAGE);
            } catch (IllegalAccessException ex) {
            JOptionPane.showMessageDialog(null, "Acceso no autorizado"+ex,"Error",JOptionPane.ERROR_MESSAGE);
            }
            
         catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Clase driver no encontrada","Error",JOptionPane.ERROR_MESSAGE);
        } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "Error de comunicaci�n a la base de datos "+ex,"Error",JOptionPane.ERROR_MESSAGE);
            }
    }

    public void insertarRegistro(String instruccionCBD) {
        try {
            Statement sentencia=null;
            sentencia=conn.createStatement();
            sentencia.executeUpdate(instruccionCBD);
            JOptionPane.showMessageDialog(null, "Registro Agregado","Hecho",JOptionPane.INFORMATION_MESSAGE);
       
        } catch (SQLException ex) {
        JOptionPane.showMessageDialog(null, "no se pudo agregar el registro"+ex,"Error :c",JOptionPane.ERROR_MESSAGE);
        }
    
    }

    public void cerrarConexion() {
        try {
            conn.close();
        } catch (SQLException ex) {
        
        }
    }

    public ResultSet consultarRegistro(String InstruccionBD) {
        
        Statement sentencia=null;
        ResultSet cdr=null;
        try {
            
            sentencia=conn.createStatement();
             cdr=sentencia.executeQuery(InstruccionBD);
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null,"no se puede ejecutar la consulta","consulta",JOptionPane.ERROR_MESSAGE);
        }
    return cdr;
    }

    public void eliminarRegistro(String instruccionBD) {
    Statement sentencia=null;
        try {
            sentencia=conn.createStatement();
            sentencia.execute(instruccionBD);
            JOptionPane.showMessageDialog(null, "Eliminado","Query ok",JOptionPane.INFORMATION_MESSAGE);
        
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No se pudo eliminar"+ex,"Error",JOptionPane.ERROR_MESSAGE);
        
        }
    }

    public void modificar(String instruccionBD) {
   Statement sentencia=null;
        try {
            sentencia=conn.createStatement();
                sentencia.execute(instruccionBD);
            JOptionPane.showMessageDialog(null, "Campos actualizados","Query ok",JOptionPane.INFORMATION_MESSAGE);
        
        } catch (SQLException ex) {
         JOptionPane.showMessageDialog(null, "No se pudo actualizar"+ex,"Error",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    
    
}
