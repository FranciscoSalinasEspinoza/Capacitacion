package com.pearbit.nivel4.vista;

import java.awt.EventQueue;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class VistaPerso extends JFrame {

	private JPanel contentPane;
	private JButton btnLlamar;
	private JButton btnAgregar;
	private JTextField jtfNombre;
	private JTextField jtfApaterno;
	private JTextField jtfAmaterno;
	private JTextField jtfEdad;
	private JTextField jtfTelefono;
	private JTextField jtfCodigo;
	private JTextField jtfTelefonia;
	private JComboBox jcbSexo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VistaPerso frame = new VistaPerso();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VistaPerso() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(38, 22, 70, 15);
		contentPane.add(lblNombre);
		
		JLabel lblApellidoPaterno = new JLabel("Apellido Paterno");
		lblApellidoPaterno.setBounds(38, 78, 143, 15);
		contentPane.add(lblApellidoPaterno);
		
		JLabel lblApellidoMaterno = new JLabel("Apellido Materno");
		lblApellidoMaterno.setBounds(38, 132, 121, 15);
		contentPane.add(lblApellidoMaterno);
		
		JLabel lblEdad = new JLabel("Edad");
		lblEdad.setBounds(38, 183, 70, 15);
		contentPane.add(lblEdad);
		
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setBounds(38, 238, 70, 15);
		contentPane.add(lblSexo);
		
		JLabel lblNumeroDeTelfono = new JLabel("Numero de teléfono");
		lblNumeroDeTelfono.setBounds(38, 283, 153, 15);
		contentPane.add(lblNumeroDeTelfono);
		
		JLabel lblCdigoDePas = new JLabel("Código de país");
		lblCdigoDePas.setBounds(38, 332, 121, 15);
		contentPane.add(lblCdigoDePas);
		
		JLabel lblTelefona = new JLabel("Telefonía");
		lblTelefona.setBounds(38, 376, 70, 15);
		contentPane.add(lblTelefona);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(88, 474, 117, 25);
		contentPane.add(btnAgregar);
		
		btnLlamar = new JButton("Llamar");
		btnLlamar.setBounds(372, 474, 117, 25);
		contentPane.add(btnLlamar);
		
		jtfNombre = new JTextField();
		jtfNombre.setBounds(446, 20, 114, 19);
		contentPane.add(jtfNombre);
		jtfNombre.setColumns(10);
		
		jtfApaterno = new JTextField();
		jtfApaterno.setBounds(446, 76, 114, 19);
		contentPane.add(jtfApaterno);
		jtfApaterno.setColumns(10);
		
		jtfAmaterno = new JTextField();
		jtfAmaterno.setBounds(446, 130, 114, 19);
		contentPane.add(jtfAmaterno);
		jtfAmaterno.setColumns(10);
		
		jtfEdad = new JTextField();
		jtfEdad.setBounds(446, 181, 114, 19);
		contentPane.add(jtfEdad);
		jtfEdad.setColumns(10);
		
		jcbSexo = new JComboBox();
		jcbSexo.setModel(new DefaultComboBoxModel(new String[] {"Seleccionar...", "M", "F"}));
		jcbSexo.setBounds(446, 233, 114, 24);
		contentPane.add(jcbSexo);
		
		jtfTelefono = new JTextField();
		jtfTelefono.setBounds(446, 281, 114, 19);
		contentPane.add(jtfTelefono);
		jtfTelefono.setColumns(10);
		
		jtfCodigo = new JTextField();
		jtfCodigo.setBounds(446, 330, 114, 19);
		contentPane.add(jtfCodigo);
		jtfCodigo.setColumns(10);
		
		jtfTelefonia = new JTextField();
		jtfTelefonia.setBounds(446, 374, 114, 19);
		contentPane.add(jtfTelefonia);
		jtfTelefonia.setColumns(10);
	}
	
	public void onClickbtnGuardar(ActionListener l) {
		btnAgregar.addActionListener(l);
	}
	public void onClickBtnLlamar(ActionListener l) {
		btnLlamar.addActionListener(l);
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public void setContentPane(JPanel contentPane) {
		this.contentPane = contentPane;
	}

	public JButton getBtnLlamar() {
		return btnLlamar;
	}

	public void setBtnLlamar(JButton btnLlamar) {
		this.btnLlamar = btnLlamar;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public JTextField getJtfNombre() {
		return jtfNombre;
	}

	public void setJtfNombre(JTextField jtfNombre) {
		this.jtfNombre = jtfNombre;
	}

	public JTextField getJtfApaterno() {
		return jtfApaterno;
	}

	public void setJtfApaterno(JTextField jtfApaterno) {
		this.jtfApaterno = jtfApaterno;
	}

	public JTextField getJtfAmaterno() {
		return jtfAmaterno;
	}

	public void setJtfAmaterno(JTextField jtfAmaterno) {
		this.jtfAmaterno = jtfAmaterno;
	}

	public JTextField getJtfEdad() {
		return jtfEdad;
	}

	public void setJtfEdad(JTextField jtfEdad) {
		this.jtfEdad = jtfEdad;
	}

	public JTextField getJtfTelefono() {
		return jtfTelefono;
	}

	public void setJtfTelefono(JTextField jtfTelefono) {
		this.jtfTelefono = jtfTelefono;
	}

	public JTextField getJtfCodigo() {
		return jtfCodigo;
	}

	public void setJtfCodigo(JTextField jtfCodigo) {
		this.jtfCodigo = jtfCodigo;
	}

	public JTextField getJtfTelefonia() {
		return jtfTelefonia;
	}

	public void setJtfTelefonia(JTextField jtfTelefonia) {
		this.jtfTelefonia = jtfTelefonia;
	}

	public JComboBox getJcbSexo() {
		return jcbSexo;
	}

	public void setJcbSexo(JComboBox jcbSexo) {
		this.jcbSexo = jcbSexo;
	}
	
	
	
}
