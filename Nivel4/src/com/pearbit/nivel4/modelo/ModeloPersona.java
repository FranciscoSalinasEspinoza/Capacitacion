package com.pearbit.nivel4.modelo;

public class ModeloPersona {
	String nombre, apellido_paterno,apellido_materno,sexo,telefono,telefonia;
	int codigo,edad;
	
	
	public ModeloPersona() {
		super();
	}
	public String getNombre() {
		return nombre;
	}
	
	public String getTelefonia() {
		return telefonia;
	}
	public void setTelefonia(String telefonia) {
		this.telefonia = telefonia;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido_paterno() {
		return apellido_paterno;
	}
	public void setApellido_paterno(String apellido_paterno) {
		this.apellido_paterno = apellido_paterno;
	}
	public String getApellido_materno() {
		return apellido_materno;
	}
	public void setApellido_materno(String apellido_materno) {
		this.apellido_materno = apellido_materno;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	
	
}
